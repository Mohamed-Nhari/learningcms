<?php

namespace App\Form;

use App\Entity\Badge;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class BadgeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'attr' => [
                    'class' => 'form-control'
                ],
                ])
            ->add('picture_name', TextType::class,[
                'attr' => [
                    'class' => 'form-control'
                ],
            ])
            ->add('picture', VichImageType::class,[
                'required' => false,
                'allow_delete' => false,
                'download_label' => '...',
                'download_uri' => false,
                'image_uri' => true
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'class' => 'form-control',
                    'rows' => '1'
                ],
            ])
            ->add('attribute', ChoiceType::class, [
                'choices' => [
                    'auto' => 1,
                    'man' => 2,
                ], 'choice_label' => function ($att, $key) {
                    if (1 == $att) {
                        return 'Attribuer automatiquement';
                    } elseif (2 == $att) {
                        return 'Attribuer manuellement';
                    }
                    return($key);
                },
                'expanded' => true,
            ])
            ->add('valid',CheckboxType::class,[
                'attr' => [
                    'class' => 'custom-control-input',
                ],
            ])
            ->add('period', TextType::class, [
                'attr' => [
                    'class' => 'per-bad',
                ],
            ])
            ->add('duty', ChoiceType::class,[
                'attr' => [
                    'class' => 'select2 small-select',
                ],
                'choices' => [
                    'CHOISIR' => '',
                    'ACQUIS' => 'acquis',
                    'EN COURS DACQUISITION' => 'en cours',
                    'NON ACQUIS' => 'non acquis',
                ]
            ])
            ->add('val_duty', TextType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev-badg',
                ]
            ])
            ->add('val_valid', ChoiceType::class, [
                'choices' => [
                    'français' => 'français',
                    'mathématiques' => 'mathématiques',
                ], 'choice_label' => function ($att, $key) {
                    if ('français' == $att) {
                        return 'Français';
                    } elseif ('mathématiques' == $att) {
                        return 'Mathématiques';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('quiz', ChoiceType::class,[
                'attr' => [
                    'class' => 'select2 small-select text-uppercase',
                ],
                'choices' => [
                    'CHOISIR LE QUIZ' => '',
                    'ACQUIS' => 'acquis',
                    'EN COURS DACQUISITION' => 'en cours',
                    'NON ACQUIS' => 'non acquis',
                ]
            ])
            ->add('val_quiz', TextType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev',
                ]
            ])
            ->add('score', IntegerType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev',
                ]
            ])
            ->add('time', IntegerType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev',
                ]
            ])
            ->add('val_time', ChoiceType::class,[
                'attr' => [
                    'class' => 'select2 small-select text-uppercase',
                ],
                'choices' => [
                    'SLECTIONNER' => '',
                    'LA PLATEFORME' => 'la plateforme',
                    'FORMATION 1' => 'formation1',
                    'FORMATION 2' => 'formation2',
                    'FORMATION 3' => 'formation3',
                    'FORUM' => 'forum',
                ]
            ])
            ->add('forum', IntegerType::class, [
                'attr' => [
                    'class' => 'custom-control-label-dev',
                ]
            ])
            ->add('note',CheckboxType::class,[
                'data' => true,
            ])
            ->add('rewards',ChoiceType::class,[
                'choices' => [
                    'Badge 1' => 'badge 1',
                    'Badge 2' => 'badge 2',
                    'Badge 3' => 'badge 3',
                    'Badge 4' => 'badge 4',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Badge 1' == $att) {
                        return 'Badge 1';
                    } elseif ('Badge 2' == $att) {
                        return 'Badge 2';
                    } elseif ('Badge 3' == $att) {
                        return 'Badge 3';
                    } elseif ('Badge 4' == $att) {
                        return 'Badge 4';
                    }
                    return($key);
                    },
                    'multiple' => true,
                    'expanded' => true,
            ])
            ->add('certifs',ChoiceType::class,[
                'choices' => [
                    'Certificat 1' => 'certificat 1',
                    'Certificat 2' => 'certificat 2',
                    'Certificat 3' => 'certificat 3',
                    'Certificat 4' => 'certificat 4',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Certificat 1' == $att) {
                        return 'Certificat 1';
                    } elseif ('Certificat 2' == $att) {
                        return 'Certificat 2';
                    } elseif ('Certificat 3' == $att) {
                        return 'Certificat 3';
                    } elseif ('Certificat 4' == $att) {
                        return 'Certificat 4';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('note_val', ChoiceType::class,[
                'attr' => [
                    'class' => 'select2 small-select text-uppercase',
                ],
                'choices' => [
                    'DE MINIMUM' => '',
                    'ACQUIS' => 'acquis',
                    'EN COURS DACQUISITION' => 'en cours',
                    'NON ACQUIS' => 'non acquis',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Badge::class,
        ]);
    }
}
