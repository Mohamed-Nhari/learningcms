<?php

namespace App\Form;

use App\Entity\Preferences;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreferencesType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mail_config', ChoiceType::class, [
                'choices' => [
                    'notif1' => 1,
                    'notif2' => 2,
                    'notif3' => 3,
                    'notif4' => 4,
                    'notif5' => 5,
                    'notif6' => 6,
            ],
                'choice_label' => function ($choice, $key) {
                    if (1 === $choice) {
                        return 'Pour les nouveaux messages';
                    } elseif (2 == $choice) {
                        return 'Pour les notiﬁcations sur la plateforme';
                    }
                    elseif (3 == $choice) {
                        return 'Pour les réponses à mes messages sur le forum';
                    }
                    elseif (4 == $choice) {
                        return 'Pour mon planning de révisions';
                    }
                    elseif (5 == $choice) {
                        return 'A l\'ajout de live';
                    }
                    elseif (6 == $choice) {
                        return 'A l\'ajout de rendez-vous';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true
            ])
            ->add('video_config', ChoiceType::class, [
                'choices' => [
                    'vid1' => 1,
                    'vid2' => 2,
                    'vid3' => 3
                ], 'choice_label' => function ($video, $key) {
                    if (1 == $video) {
                        return 'Lors de la prise de note arrêter automatiquement la vidéo jusqu\'à taper sur la barre espace et la touche L du clavier en simultanée .';
                    } elseif (2 == $video) {
                        return 'Lors de la prise de note arrêter automatiquement la vidéo puis reprendre après 3 secondes d\'inactivité';
                    } elseif (3 === $video) {
                        return 'Lors de la prise de note arrêter automatiquement la vidéo et reprendre apres 6 secondes d\'inactivité';
                    }
                    return($key);
                },
                'expanded' => true,
                'multiple' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Preferences::class,
        ]);
    }
}
