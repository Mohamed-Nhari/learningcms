<?php

namespace App\Form;

use App\Entity\Code;
use App\Form\DataTransformer\ArrayToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

class CodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new ArrayToStringTransformer();
        $builder
            ->add('title', TextType::class,[
                'attr' => [
                    'class' => 'form-control'
                ],
            ])
            ->add('description', TextareaType::class,[
                'attr' => [
                    'class' => 'form-control',
                    'rows' => '1'
                ],
            ])
            ->add('quantity',IntegerType::class,[
                'attr' => [
                'class' => 'form-control'
                    ],
                ])
            ->add('formation',ChoiceType::class,[
                'choices' => [
                    'Formation 1' => 'Formation 1',
                    'Formation 2' => 'Formation 2',
                    'Formation 3' => 'Formation 3',
                    'Formation 4' => 'Formation 4',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Formation 1' == $att) {
                        return 'Formation 1';
                    } elseif ('Formation 2' == $att) {
                        return 'Formation 2';
                    } elseif ('Formation 3' == $att) {
                        return 'Formation 3';
                    } elseif ('Formation 4' == $att) {
                        return 'Formation 4';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true,
                'compound' => true,
            ])
            ->add('class',ChoiceType::class,[
                'choices' => [
                    'Classe 1' => 'Classe 1',
                    'Classe 2' => 'Classe 2',
                    'Classe 3' => 'Classe 3',
                    'Classe 4' => 'Classe 4',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Classe 1' == $att) {
                        return 'Classe 1';
                    } elseif ('Classe 2' == $att) {
                        return 'Classe 2';
                    } elseif ('Classe 3' == $att) {
                        return 'Classe 3';
                    } elseif ('Classe 4' == $att) {
                        return 'Classe 4';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true,
                'compound' => true,
            ])
            ->add('statut',ChoiceType::class,[
                'choices' => [
                    'Étudiant' => 'Étudiant',
                    'Éditeur' => 'Éditeur',
                    'Formateur' => 'Formateur',
                ],
                'choice_label' => function ($att, $key) {
                    if ('Étudiant' == $att) {
                        return 'Étudiant';
                    } elseif ('Éditeur' == $att) {
                        return 'Éditeur';
                    } elseif ('Formateur' == $att) {
                        return 'Formateur';
                    }
                    return($key);
                },
                'multiple' => true,
                'expanded' => true,
                'compound' => true,
            ])
            ->add('period', TextType::class,[
                'attr' => [
                    'class' => 'form-control'
                ],
            ])
            ->addModelTransformer($transformer)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Code::class,
        ]);
    }
}
