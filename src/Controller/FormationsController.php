<?php


namespace App\Controller;

use App\Entity\Badge;
use App\Entity\Category;
use App\Entity\Code;
use App\Entity\Configchapitre;
use App\Entity\Configcours;
use App\Entity\NewUser;
use App\Entity\Chapters;
use App\Entity\Configmodule;
use App\Entity\Cours;
use App\Entity\Module;
use App\Entity\User;
use App\Form\BadgeType;
use App\Form\CategoryType;
use App\Form\CodeType;
use App\Form\NewUserType;
use App\Form\ProfileType;
use App\Repository\BadgeRepository;
use App\Repository\CategoryRepository;
use App\Repository\CodeRepository;
use App\Repository\FormationconfigRepository;
use App\Repository\NewUserRepository;
use App\Entity\Formation;
use App\Entity\Formationconfig;
use App\Form\FormationconfigType;
use App\Form\FormationType;
use App\Repository\DocumentRepository;
use App\Repository\FormationRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("formation/")
 */
class FormationsController extends AbstractController
{
    /**
     * @Route("step1", name="new_formation_step1")
     */
    public function step1(ObjectManager $manager, Request $request, DocumentRepository $media, CategoryRepository $categoryRepository)
    {
        $categories = $categoryRepository->findAll();
        $formation = new Formation();
        $formationId=null;
        //$formationRepository = $formationRepository->find($formationId);
        $form = $this->createForm(FormationType::class, $formation);

        $form->handleRequest($request);
        $user = $this->getUser();
        $userMedia   = $media->findBy(array('user' => $user ));
       // dump($formationRepository);
        if ($form->isSubmitted() && $form->isValid()) {

            $formation->setUser($user);

            $manager->persist($formation);

            $manager->flush();
            return $this->redirectToRoute('new_formation_step2', ['formationId' => $formation->getId()]);

        }

        return $this->render('formations/step_1.html.twig', [
            'form' => $form->createView(),
            'toggled' => true,
            'userMedia' => $userMedia,
            'categories' => $categories,
        ]);
    }
    /**
     * @Route("step2/{formationId}", name="new_formation_step2")
     */
    public function step2(ObjectManager $manager, Request $request, FormationRepository $formationRepository, FormationconfigRepository $formationstep2update, $formationId)
    {
        $formationRepository = $formationRepository->find($formationId);
        $nameformation = $formationRepository->getTitreFormation();

        $config = $formationRepository->getConfigformation();

        if(!empty($config)) {
            $configFormation = $formationstep2update->findOneBy(array('id' =>$config->getId()));
        } else {
            $configFormation = new Formationconfig();
        }

        $forms = $this->createForm(FormationconfigType::class, $configFormation);

        $forms->handleRequest($request);
        if ($forms->isSubmitted()) {

            //$configFormation->setLanguage('Anglais');
            $manager->persist($configFormation);
            $manager->flush();
            return $this->redirectToRoute('new_formation_step3', ['formationId' => $formationId]);
        }
        return $this->render('formations/step_2.html.twig', [
            'form' => $forms->createView(),
            'toggled' => true,
            'nameformation'=>$nameformation,
        ]);
    }

    /**
     * @Route("step3/{formationId}", name="new_formation_step3")
     */
    public function step3($formationId, Request $request, ObjectManager $manager,FormationRepository $formationRepository,DocumentRepository $media)
    {
  $formationRepository = $formationRepository->find($formationId);
   $nameformation=$formationRepository->getTitreFormation();
        $user = $this->getUser();
        $userMedia   = $media->findBy(array('user' => $user ));
        $formation = $this->getDoctrine()->getRepository(Formation::class)->find($formationId);
        $datas = $request->get('url_to_send');
        $modules = json_decode($datas, true);
//dd($modules);

        if ($data = $request->get('moduleconfig_forms')) {
            $jsonData = $request->get('moduleconfig_forms');
           
 
            foreach ($jsonData as $indexs => $modules) {
                $emptyModuleConfig = 0;
                if ($modules['type'] == "Module ") {
                    //dd($jsonData);
                    $moduleConfig = new Configmodule();

                    if(isset($modules['score'])) {
                        $moduleConfig->setscore($modules['score']);
                        $emptyModuleConfig = 1;
                    }
                    if(isset($modules['statut_score'])) {
                        $moduleConfig->setStatutScore($modules['statut_score']);
                        $emptyModuleConfig = 1;
                    }
                    if(isset($modules['access'])) {
                        $moduleConfig->setAccess($modules['access']);
                        $emptyModuleConfig = 1;
                    }
                    if(isset($modules['access_input'])) {
                        $moduleConfig->setAccessInput($modules['access_input']);
                        $emptyModuleConfig = 1;
                    }
                    if(isset($modules['access_subscribe'])) {
                        $moduleConfig->setAccesDays($modules['access_subscribe']);
                        $emptyModuleConfig = 1;
                    }
                    if(isset($modules['statut_prequis'])) {
                        $moduleConfig->setStatutPrequis($modules['statut_prequis']);
                        $emptyModuleConfig = 1;
                    }
                    if(isset($modules['icone'])) {
                        $moduleConfig->setIcone($modules['icone']);
                        $emptyModuleConfig = 1;
                    }
                    if(isset($modules['point_req'])) {
                        $moduleConfig->setPointPreq($modules['point_req']);
                        $emptyModuleConfig = 1;
                    }
                    if(isset($modules['choice_preq'])) {
                        $moduleConfig->setChoicePreq($modules['choice_preq']);
                        $emptyModuleConfig = 1;
                    }
                    if(isset($modules['icone_image'])) {
                        $moduleConfig->setIconeImage($modules['icone_image']);
                        $emptyModuleConfig = 1;
                    }
                    $module = new Module();
                    if($emptyModuleConfig == 1) {
                        $manager->persist($moduleConfig);
                        $manager->flush();
                        $module->setConfigmodule($moduleConfig);
                    }

                    $module->setName($modules['name']);
                    $module->setFormation($formation);

                    $manager->persist($module);
                    $manager->flush();
                }

                if ($modules['type'] == "Cours " && $modules['parentNodes'][0] != "") {
                    $coursConfig= new Configcours();
                    $emptyCoursConfig = 0;
                    if(isset($modules['score'])) {
                        $coursConfig->setScore($modules['score']);
                        $emptyCoursConfig = 1;
                    }
                    if(isset($modules['score_point'])) {
                        $coursConfig->setScorePoint($modules['score_point']);
                        $emptyCoursConfig = 1;
                    }
                    if(isset($modules['statut_access'])) {
                        $coursConfig->setStatutAccess($modules['statut_access']);
                        $emptyCoursConfig = 1;
                    }
                    if(isset($modules['date_access'])) {
                        $coursConfig->setDateAccess($modules['date_access']);
                        $emptyCoursConfig = 1;
                    }
                    if(isset($modules['access_subscribe'])) {
                        $coursConfig->setAccessSubscribe($modules['access_subscribe']);
                        $emptyCoursConfig = 1;
                    }
                    if(isset($modules['day_acces'])) {
                        $coursConfig->setDayAcces($modules['day_acces']);
                        $emptyCoursConfig = 1;
                    }
                    if(isset($modules['statuts_prerequis'])) {
                        $coursConfig->setStatutsPrerequis($modules['statuts_prerequis']);
                        $emptyCoursConfig = 1;
                    }
                    if(isset($modules['select_choiceprerequis'])) {
                        $coursConfig->setSelectChoiceprerequis($modules['select_choiceprerequis']);
                        $emptyCoursConfig = 1;
                    }
                  //  $coursconfig->setNumberpointsPrerequis($modules['numberpoints_prerequis']);
                    $cours = new Cours();
                    if($emptyCoursConfig == 1) {
                        $manager->persist($coursConfig);
                        $manager->flush();
                        $cours->setConfig($coursConfig);
                    }
                    $cours->setName($modules['name']);
                    $cours->setModule($module);
                    $manager->persist($cours);
                    $manager->flush();
                }
                if ($modules['type'] == "Chap " && $modules['parentNodes'][0] != "") {
                    $chapitrecongig = new Configchapitre();
                    $emptyChapter = 0;
                    if(isset($modules['picture'])) {
                        $chapitrecongig->setPicture($modules['picture']);
                        $emptyChapter = 1;
                    }
                    if(isset($modules['time_spent'])) {
                        $chapitrecongig->setTimeSpent($modules['time_spent']);
                        $emptyChapter = 1;
                    }
                    if(isset($modules['status_completed'])) {
                        $chapitrecongig->setStatusCompleted($modules['status_completed']);
                        $emptyChapter = 1;
                    }
                    if(isset($modules['score'])) {
                        $chapitrecongig->setScore($modules['score']);
                        $emptyChapter = 1;
                    }
                    if(isset($modules['score_point'])) {
                        $chapitrecongig->setScorePoint($modules['score_point']);
                        $emptyChapter = 1;
                    }
                    if(isset($modules['statut_accesss'])) {
                        $chapitrecongig->setStatutAccesss($modules['statut_accesss']);
                        $emptyChapter = 1;
                    }
                    if(isset($modules['status_dateacces'])) {
                        $chapitrecongig->setStatusDateacces($modules['status_dateacces']);
                        $emptyChapter = 1;
                    }
                    if(isset($modules['staut_subscribe'])) {
                        $chapitrecongig->setStautSubscribe($modules['staut_subscribe']);
                        $emptyChapter = 1;
                    }
                    if(isset($modules['staut_datesubscribe'])) {
                        $chapitrecongig->setStautDatesubscribe($modules['staut_datesubscribe']);
                        $emptyChapter = 1;
                    }
                    if(isset($modules['staut_prerequis'])) {
                        $chapitrecongig->setStautPrerequis($modules['staut_prerequis']);
                        $emptyChapter = 1;
                    }
                    $chap = new Chapters();
                    if($emptyChapter == 1) {
                        $manager->persist($chapitrecongig);
                        $manager->flush();
                        $chap->setConfig($chapitrecongig);
                    }
                    $chap->setName($modules['name']);
                    $chap->setCours($cours);
                    $manager->persist($chap);
                    $manager->flush();
                }
                if ($modules['type'] == "Cours " && $modules['parentNodes'] == "") {
                    $cours = new Cours();
                    $cours->setName($modules['name']);
                    $cours->setFormation($formation);
                    $manager->persist($cours);
                    $manager->flush();
                }

                if ($modules['type'] == "Chap " && $modules['parentNodes'] == "") {

                    $chap = new Chapters();
                    $chap->setName($modules['name']);
                    $chap->setFormation($formation);
                    $manager->persist($chap);
                    $manager->flush();
                }
            }
            //return $this->redirectToRoute('new_formation_step5carnetdenote', ['formationId' => $formationId]);
        }

        return $this->render('formations/step_3_v2.html.twig',
            ['toggled' => true,
                'formationId' => $formationId,
                'nameformation'=>$nameformation,
                'userMedia' => $userMedia,
            ]);
    }
    /**
     * @Route("step3V2/{formationId}", name="new_formation_step3v2")
     */
    public function step3V2()
    {

        return $this->render('formations/step_3.html.twig', ['toggled' => true]);
    }
    /**
     * @Route("configformation", name="new_formation_configformation")
     */
    public function step4()
    {
        return $this->render('formations/configformation.html.twig', ['toggled' => true]);
    }


    /**
     * @Route("chaiptre", name="new_formation_chaiptre")
     */
    public function step4etape1() {

        return $this->render('formations/configformationchaiptrea.html.twig',['toggled' => true]);
    }

    /**
     * @Route("generalconfigchaiptre", name="new_formation_generalconfigchaiptre")
     */
    public function step4etape3() {

        return $this->render('formations/generalconfigchaiptre.html.twig',['toggled' => true]);
    }

    /**
     * @Route("cours", name="new_formation_cours")
     */
    public function step4etape2() {

        return $this->render('formations/configformationcour.html.twig',['toggled' => true]);
    }

    /**
     * @Route("configformationquiz", name="new_formation_configformationquiz")
     */
    public function step4etape4() {

        return $this->render('formations/configquiz.html.twig',['toggled' => true]);
    }

    /**
     * @Route("step5carnetdenote{formationId}", name="new_formation_step5carnetdenote")
     */
    public function step5($formationId) {

        return $this->render('formations/step5carnetdenote.html.twig',
        [
            'toggled' => true,
            'formationId' => $formationId
            ]);
    }

    /**
     * @Route("step6", name="new_formation_step6")
     */
    public function step6(Request $request, BadgeRepository $badgeRepository) {

        $badges = $badgeRepository->findAll();
        return $this->render('formations/step_6.html.twig',[
            'badges' => $badges,
        ]);
    }
    /**
     * @Route("Badgestep6", name="new_badge_step6")
     */
    public function badgeStep6(ObjectManager $manager, Request $request, BadgeRepository $badgeRepository)
    {
        $badge = new Badge();
        $form = $this->createForm(BadgeType::class, $badge);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($badge);

            $manager->flush();

            return $this->redirectToRoute('new_formation_step6');
        }
        return $this->render('formations/step_6_badge.html.twig',[
            'form' => $form->createView(),
            'toggled' => true,
        ]);
    }
    /**
     * delete list of selected badges
     * @Route("deleteBadges", name="delete_badges")
     */
    public function deleteBadges(ObjectManager $entityManager, Request $request, BadgeRepository $badgeRepository) {
        $badges = $request->request->get('badges');

        if(!empty($badges)) {
            foreach ($badges as $key => $badge) {
                $currentBadge = $badgeRepository->find($badge);
                $entityManager->remove($currentBadge);
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        }else {
            return new Response(
                'error'
            );
        }

    }
    /**
     * delete the selected badge
     * @Route("deleteBadge", name="delete_badge")
     */
    public function deleteBadge(Request $request, ObjectManager $manager, BadgeRepository $badgeRepository) {
        $id  = $request->request->get('id');
        $current_badge   = $badgeRepository->findOneBy(array('id' => $id ));
        if($current_badge) {
            $manager->remove( $current_badge);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }
    /**
     * duplicate the selected badge
     * @Route("duplicateBadge/{id}", name="duplicate_badge")
     */
    public function duplicateBadge(ObjectManager $manager, BadgeRepository $badgeRepository, Badge $badge) {

        $newBadge = new Badge();
        if($badge) {
            $newBadge->setName($badge->getName())
                     ->setPicture($badge->getPicture())
                     ->setDescription($badge->getDescription())
                     ->setAttribute($badge->getAttribute())
                     ->setValid($badge->getValid())
                     ->setPeriod($badge->getPeriod())
                     ->setDuty($badge->getDuty())
                     ->setValidDuty($badge->getValidDuty())
                     ->setQuiz($badge->getQuiz())
                     ->setScore($badge->getScore())
                     ->setTime($badge->getTime())
                     ->setForum($badge->getForum())
                     ->setNote($badge->getNote())
                     ->setRewards($badge->getRewards());

            $manager->persist($newBadge);

            $manager->flush();

            $this->addFlash('success',
                'Votre note a été dupliquée avec succée!');

        }
        return $this->redirectToRoute('new_formation_step6');
    }
    /**
     * delete list of selected categories
     * @Route("deleteCategories", name="delete_categories")
     */
    public function deleteCategories(ObjectManager $entityManager, Request $request, CategoryRepository $categoryRepository) {
        $categories = $request->request->get('categories');

        if(!empty($categories)) {
            foreach ($categories as $key => $categorie) {
                $currentCategorie = $categoryRepository->find($categorie);
                $entityManager->remove($currentCategorie);
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        }else {
            return new Response(
                'error'
            );
        }

    }
    /**
     * delete list of selected formations
     * @Route("deleteFormations", name="delete_formations")
     */
    public function deleteFormations(ObjectManager $entityManager, Request $request, FormationRepository $formationRepository) {
        $formations = $request->request->get('formations');

        if(!empty($formations)) {
            foreach ($formations as $key => $formation) {
                $currentFormation = $formationRepository->find($formation);
                $entityManager->remove($currentFormation);
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        }else {
            return new Response(
                'error'
            );
        }

    }
    /**
     * delete the selected category
     * @Route("deleteCategory", name="delete_category")
     */
    public function deleteCategory(Request $request, ObjectManager $manager, CategoryRepository $categoryRepository) {
        $id  = $request->request->get('id');
        $current_category   = $categoryRepository->findOneBy(array('id' => $id ));
        if($current_category) {
            $manager->remove($current_category);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }
    /**
     * delete the selected formation
     * @Route("deleteFormation", name="delete_formation")
     */
    public function deleteFormation(Request $request, ObjectManager $manager, FormationRepository $formationRepository) {
        $id  = $request->request->get('id');
        $current_formation   = $formationRepository->findOneBy(array('id' => $id ));
        if($current_formation) {
            $manager->remove($current_formation);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }
    /**
     * @Route("step7", name="new_formation_step7")
     */
    public function step7() {

        return $this->render('formations/step7.html.twig',['toggled' => true]);
    }

     /**
     * @Route("addUsersToFormation", name="add_users_to_formation")
     */
    public function addUsersToFormation(ObjectManager $manager, Request $request, UserRepository $newUserRepository) {

        $newUser = new User();
        $form = $this->createForm(ProfileType::class, $newUser);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($newUser);

            $manager->flush();

            return $this->redirectToRoute('add_users_to_formation');
        }
        return $this->render('formations/addUsers.html.twig',[
            'form' => $form->createView(),'toggled' => true,
        ]);
    }
    /**
     * @Route("newCode", name="new_code")
     */
    public function newCode(ObjectManager $manager, Request $request, CodeRepository $codeRepository) {

        $code = new Code();
        $form = $this->createForm(CodeType::class, $code);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($code);

            $manager->flush();

            return $this->redirectToRoute('code_gen', ['id' => $code->getId()]);
        }
        return $this->render('formations/new_code.html.twig',[
            'form' => $form->createView(),
            'toggled' => true,
        ]);
    }
    /**
     * @Route("code/{id}", name="edit_code")
     */
    public function editCode(ObjectManager $manager, Request $request, $id, CodeRepository $codeRepository) {

        $code = $codeRepository->findBy(array('id' =>  $id ));

        $form = $this->createForm(CodeType::class, $code);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->flush();
            $this->addFlash(
                'success',
                'Votre code a été mise à jour!'
            );

            return $this->redirectToRoute('code_gen', ['id' => $id]);
        }
        return $this->render('formations/new_code.html.twig',[
            'form' => $form->createView(),
            'toggled' => true,
        ]);
    }
    /**
     * @Route("createCertification", name="new_inscrip_step7")
     */
    public function createCertification()
    {
        return $this->render('formations/step_6_certificat.html.twig',['toggled' => true]);
    }

    /**
     * @Route("Step7code", name="new_code_step7")
     */
    public function Step7code()
    {
        return $this->render('formations/step7_code.html.twig',['toggled' => true]);
    }
    /**
     * @Route("Step7users", name="new_user_step7")
     */
    public function Step7users(UserRepository $repo)
    {
        $users = $repo->findAll();
        return $this->render('formations/step7_users.html.twig',['users' => $users]);
    }
    /**
     * @Route("codegen/{id}", name="code_gen")
     */
    public function Codegen(ObjectManager $manager, Request $request, $id,CodeRepository $codeRepository)
    {
        $code = $codeRepository->findBy(array('id' =>  $id ));
        return $this->render('formations/step7_v4.html.twig',[
            'code' => $code,
            'toggled' => true
        ]);
    }
    /**
     * @Route("home", name="home_formation")
     */
    public function Home(ObjectManager $manager, Request $request, CategoryRepository $categoryRepository, FormationRepository $formationRepository) {

        $categories = $categoryRepository->findAll();
        $formations = $formationRepository->findAll();
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($category);

            $manager->flush();

            return $this->redirectToRoute('home_formation');
        }
        return $this->render('formations/home.html.twig',[
            'form' => $form->createView(),
            'categories' => $categories,
            'formations' => $formations,
            'toggled' => true,
        ]);
    }
    /**
     * duplicate the selected category
     * @Route("duplicateCategory/{id}", name="duplicate_category")
     */
    public function duplicateCategory(ObjectManager $manager, CategoryRepository $categoryRepository, $id, Category $category) {

        $newCategory = new Category();
        if($category) {
            $newCategory->setName($category->getName());

            $manager->persist($newCategory);

            $manager->flush();

            $this->addFlash('success',
                'Votre categorie a été dupliquée avec succée!');

        }
        return $this->redirectToRoute('home_formation');
    }
    /**
     * @Route("newForm", name="new_formation")
     */
    public function NewFormation(FormationRepository $formationRepository)
    {
        $formations = $formationRepository->findAll();
        return $this->render('formations/new.html.twig',[
            'toggled' => true,
            'formations' => $formations,
            ]);
    }
    /**
     * @Route("newModule", name="show_moduleForm")
     */
    public function NewModule()
    {
        return $this->render('formations/new_module.html.twig',['toggled' => true]);
    }

    public static function get_object($name_module)
    {
        if (strpos($name_module, 'Module')) {
            $object = new Module();
        } elseif (strpos($name_module, 'Cours')) {
            $object = new Cours();
        }

        return $object;
    }
}