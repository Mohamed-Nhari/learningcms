<?php
namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * List all type of users and allow admin to add new users
     * @Route("/typeUser", name="type_user")
     */
    public function typeUser() {

        return $this->render('user/type_user.html.twig',['toggled' => true]);
    }

    /**
     * get list of users acoording to the given ruls if the param role is empty return list off all users
     * @Route("/admin/getUsers/{role}", name="users_list")
     */
    public function getUsers(UserRepository $repo, $role = null) {
        $type = "admin";
        $users = $repo->findAll();
        /*switch ($role) {
            case "1": $type = "admin";
            case "2": $type = "user";
            case "3": $type = "editor";
            case "4": $type = "coach";
            case "5" : $type = "formator";
            default: $type = "user";
        }*/
        //get list of all users

        return $this->render('user/list-user.html.twig',['users' => $users]);
    }

    /**
     * update the user status it can be 0 for inactive user and 1 to active user
     * @Route("/admin/changeStatus", name="update_status")
     */
    public function changeStatus(Request $request, ObjectManager $manager, UserRepository $repo) {
        $id  = $request->request->get('id');
        $status  = $request->request->get('active');
        $current_user   = $repo->findOneBy(array('id' => $id ));
        if($current_user) {
            $current_user->setActive($status);
            $manager->persist( $current_user);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * delete the selected user
     * @Route("/admin/deleteUser", name="delete_user")
     */
    public function deleteUser(Request $request, ObjectManager $manager, UserRepository $repo) {
        $id  = $request->request->get('id');
        $current_user   = $repo->findOneBy(array('id' => $id ));
        if($current_user) {
            $manager->remove( $current_user);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * delete list of selected users
     * @Route("/admin/deleteAllUser", name="delete_all_user")
     */
    public function deleteAllUser(Request $request, ObjectManager $manager, UserRepository $repo) {
        $list_users = $request->request->get('users');

        if(!empty($list_users)) {
            foreach ($list_users as $key => $user) {
                $currentUser = $repo->find($user);
                $manager->remove($currentUser);
                $manager->flush();
            }
            return new Response(
                'success'
            );
        }else {
            return new Response(
                'error'
            );
        }
    }

    /**
     *
     * @Route("/admin/activateAlluser", name="activate_all_user")
     */
    public function activateAlluser(Request $request, ObjectManager $manager, UserRepository $repo) {
        $list_users = $request->request->get('users');

        if(!empty($list_users)) {
            foreach ($list_users as $key => $user) {
                $currentUser = $repo->find($user);
                $currentUser->setActive(1);
                $manager->persist($currentUser);
                $manager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/admin/desactivateAlluser", name="desactivate_all_user")
     */
    public function desactivateAlluser(Request $request, ObjectManager $manager, UserRepository $repo) {
        $list_users = $request->request->get('users');

        if(!empty($list_users)) {
            foreach ($list_users as $key => $user) {
                $currentUser = $repo->find($user);
                $currentUser->setActive(0);
                $manager->persist($currentUser);
                $manager->flush();
            }
            return new Response(
                'success'
            );
        }else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/admin/updateCounter", name="update_counter")
     */
    public function updateCounter(Request $request, ObjectManager $manager, UserRepository $repo) {
        $counter      = $request->request->get('counter');
        $current_user = $this->getUser();
        if($current_user) {
            $oldCounter = $current_user->getCounter();
            $tmstamp = strtotime($oldCounter) + strtotime($counter);
            $current_user->setCounter(date("H:m:s", $tmstamp));
            $manager->persist( $current_user);
            $manager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

}