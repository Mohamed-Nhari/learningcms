<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Document;
use App\Entity\DocumentFolders;
use App\Repository\DocumentRepository;
use App\Repository\DocumentFoldersRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ContentLibraryController extends AbstractController
{
    /**
    *@Route("admin/contentLibrary", name="content_library")
    */
    public function contentLibrary(DocumentRepository $media, DocumentFoldersRepository $folder ) {
    	//get all content for the curret user
    	$user        = $this->getUser();
    	$userMedia   = $media->findBy(array('user' => $user ));
        //get folders 
        $folders   = $folder->findBy(array('user' => $user ), array('name' => 'ASC'));
        $arr = array_chunk($folders, 8);

    	return $this->render('admin/contentLibrary.html.twig', [
    		'userMedia' => $userMedia,'folders' => $arr,'allfolders' => $folders]);
    }

    /**
     * @Route("/ajax/snippet/image/send", name="ajax_snippet_image_send")
     */
    public function ajaxSnippetImageSendAction(Request $request, ObjectManager $manager)
    {
    	$user     = $this->getUser();
    	$random   =  random_int(1, 200);
        $document = new Document();

        $media    = $request->files->get('file');
        $document->setFile($media);
        //concatenate the file name with a randomizer value to prevent duplicate file name registration
        $document->setName($random.''.$media->getClientOriginalName());
        $document->setUser($user);
        $document->setcreatedAt(new \DateTime);
        $document->setSize($media->getsize());

        $document->upload($random);
        $manager->persist($document);
        $manager->flush();
        $id = $document->getId();

        //infos sur le document envoyé
        //var_dump($request->files->get('file'));die;
        return new JsonResponse(array('success' => true,'id' => $id));
    }

    /**
    *@Route("admin/getMedia", name="get_media")
    *@Route("admin/getMedia/{id}", name="get_media")
    */
    public function getMedia(DocumentRepository $media, DocumentFoldersRepository $folder, Request $request, $id=null) {
    	//get all content for the curret user
        $folderId = $request->request->get('folderId');
        
        $userMedia = [];

        if($folderId) {
            $currentFolder     = $folder->find($folderId);
            if($currentFolder) {
                $userMedia     = $currentFolder->getDocuments(); 
            }      
        } elseif($id) {
            $media       = $media->find($id);
            $user        = $this->getUser();
            $folders     = $folder->findBy(array('user' => $user ), array('name' => 'ASC'));
            return $this->render('admin/includes/sidebarMedia.html.twig', [
            'media' => $media, 'allfolders' => $folders]);

        }else {
            $user        = $this->getUser();
            $userMedia   = $media->findBy(array('user' => $user ));
        }
       
    	return $this->render('admin/filesView.html.twig', [
    		'userMedia' => $userMedia]);
    }

    /**
     * @Route("/admin/deleteImage", name="delete_image")
     */
    public function deleteImage(Request $request, ObjectManager $entityManager, DocumentRepository $document) {

        $mediaId   = $request->request->get('id');
        $media     = $document->find($mediaId);
        if($media) {
        	
            //delete file from server
            $user = $this->getUser();
			$dir  = __DIR__.'/../../public/images/uploads/';
			
			$imagePath = realpath($dir).'\\'.$user->getId().'\documents\\'.$media->getName();
			
			if (file_exists($imagePath) )
		    { 
		        unlink ( $imagePath );
		    }
            //delete file from database
            $entityManager->remove($media);
            $entityManager->flush();
            
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/admin/updateFile", name="updat_file")
     */
    public function updateFile(Request $request, ObjectManager $entityManager, DocumentRepository $document, DocumentFoldersRepository $folder) {

        $mediaId   = $request->request->get('id');
        $foldersIds  = $request->request->get('foldersIds');
        $media     = $document->find($mediaId);

        if($foldersIds) {
            //add media to selected folders
            $folders    = explode(',', $foldersIds);

            $oldfolder  = $media->getDocumentFolders();
            $oldFolderIds = [];
            if($oldfolder) {
                foreach ($oldfolder as $key => $f) {
                    array_push($oldFolderIds, $f->getId());
                    if(!in_array($f->getId(), $folders)) {
                        //remove the folder for the current media
                        $currentFolder     = $folder->find($f->getId());
                        $media->removeDocumentFolder($currentFolder);
                    }
                }
            }
            foreach ($folders as $k => $folderId) {
                $currentFolder     = $folder->find($folderId);
                $media->addDocumentFolder($currentFolder);
            }
        }
      
        $media->setTitle($request->request->get('title'));
        $media->setAlternatifText($request->request->get('alternativText'));
        $media->setDescription($request->request->get('description'));

       
        if($media) {
        	//update file 
            $entityManager->persist($media);
            $entityManager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/admin/addFolderDocument", name="add_folder_document")
     */
    public function addFolderDocument(Request $request, ObjectManager $entityManager) {
        $folderName = $request->request->get('folder_name');
       
        if($folderName) {
            $currentUser = $this->getUser();
            if($currentUser) {
                //add a new folder for the connected user
                $folder = new DocumentFolders();
                $folder->setName($folderName);
                $folder->setUser($currentUser);
                $entityManager->persist($folder);
                // actually executes the queries (i.e. the INSERT query)
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

     /**
     * @Route("/admin/updateFolderDoc", name="update_folder_doc")
     */
    public function updateFolderDoc(Request $request, ObjectManager $entityManager, DocumentFoldersRepository $Folder) {

        $folderName = $request->request->get('folder_name');
        $folderId   = $request->request->get('id');
        $folder     = $Folder->find($folderId);
    
        if($folder) {
            $folder->setName($folderName);
            $entityManager->persist($folder);
            $entityManager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

     /**
     * @Route("/admin/deleteFolderDoc", name="delete_folder_doc")
     */
    public function deleteFolderDoc(Request $request, ObjectManager $entityManager, DocumentFoldersRepository $Folder) {
        $folderId   = $request->request->get('id');
        $folder     = $Folder->find($folderId);
        if($folder) {
            $entityManager->remove($folder);
            $entityManager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

     /**
    *@Route("/admin/getFolder/{nb}", name="get_folder")
    */
    public function getFolder(DocumentFoldersRepository $folder, $nb) {
        //get all folders for the curret user
        $user    = $this->getUser();
        $folders = $folder->findBy(array('user' => $user ));
        $arr     = array_chunk($folders, $nb);

        return $this->render('admin/foldersView.html.twig', [
            'folders' => $arr]);
    }

    /**
     * @Route("/admin/affectFolder", name="affect_folder")
     */
    public function affectFolder(Request $request, ObjectManager $entityManager, DocumentRepository $document, DocumentFoldersRepository $folder) {

        $filesIds   = trim($request->request->get('filesIds'),",");
        $foldersIds = $request->request->get('folders');
        $files      =  explode(',', $filesIds);
        $folders    = explode(',', $foldersIds);

       
        foreach ($files as $key => $file) {
            $media     = $document->find($file);
            foreach ($folders as $k => $folderId) {
                $currentFolder     = $folder->find($folderId);
                $media->addDocumentFolder($currentFolder);
            }
        }
       //add document to a folder 
        $entityManager->persist($media);
        $entityManager->flush();
        return new Response(
            'success'
        );
    }

    /**
     * @Route("/admin/deleteFiles", name="delete_files")
     */
    public function deleteFiles(ObjectManager $entityManager, Request $request, DocumentRepository $document) {
        $files = $request->request->get('files');
      
        if(!empty($files)) {
            foreach ($files as $key => $file) {
                $currentFile = $document->find($file);
                //delete file from server
                $user = $this->getUser();
                $dir  = __DIR__.'/../../public/images/uploads/';
                
                $imagePath = realpath($dir).'\\'.$user->getId().'\documents\\'.$currentFile->getName();
                
                if (file_exists($imagePath) )
                { 
                    unlink ( $imagePath );
                }
                //remove file from database
                $entityManager->remove($currentFile);
                $entityManager->flush();

            }
            return new Response(
                'success'
            );
        }
    
    }
}