<?php

namespace App\Controller;

use App\Entity\StudentFolders;
use App\Entity\StudentNotes;
use App\Form\ProfileType;
use App\Repository\StudentFoldersRepository;
use App\Repository\StudentNotesRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotesStudentController extends AbstractController
{
    /**
     * @Route("/notes/student", name="notes_student")
     */
    public function index()
    {
        return $this->render('notes_student/index.html.twig', [
            'controller_name' => 'NotesStudentController',
        ]);
    }

    /**
     * @Route("/student/notes", name="student_note")
     */
    public function note(StudentFoldersRepository $studentFolder, Request $request, ObjectManager $manager, StudentNotesRepository $studentNote) {
        //get list of folder by user
        $user      = $this->getUser();
        $folders   = $studentFolder->findBy(array('user' => $user ));
        $userNotes = $studentNote->findBy(array('user' => $user,'status' => 1));
        $note = new StudentNotes();
        $note->setCreatedAt(new \DateTime)
             ->setType('text')
             ->setStatus(1);
             $foldersIds = [];
        if(!empty($folders)) {
            foreach ($folders as $key => $f) {
                array_push($foldersIds, $f->getId());
            }
        }

        $form = $this->createFormBuilder($note)
            ->add('title')
            ->add('studentFolder', EntityType::class, [
                'class' => StudentFolders::class,
               'choice_label'  => 'name',
                'placeholder' => 'Nom du dossier',
                'required' => false,
                'choice_label'  => function ($stfolders) {
                    if($stfolders->getUser() == $this->getUser())  return $stfolders->getName();
                },
               /* 'choice_value' => function ($stfolders) {
                      if($stfolders->getUser() == $this->getUser())  return $stfolders->getName();
                },*/

            ])
            ->add('value', CKEditorType::class,['config' => ['uiColor' => '#ffffff', 'toolbar' => [ "/", [ 'Bold','Italic','Underline','Strike', 'Blockquote','Subscript','Superscript','-' ], [ 'NumberedList','BulletedList','-','-','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ], ['Table' ], '/', [ 'Styles', 'Format','Font','FontSize' ], [ 'TextColor' ] ]]])
            ->getForm();

        $form->handleRequest($request);

            
        if($form->isSubmitted() && $form->isValid()) {
            $value = $request->request->get('form')['value'];
            if($value) {
                $note->setUser($user);
                $manager->persist( $note);
                $manager->flush();
            }
            $this->addFlash(
                'success',
                'Votre note est ajouté avec succées!'
            );
           // return $this->redirectToRoute('student_note');
        } 
        return $this->render('notes_student/note.html.twig', [
            'folders' => $folders, 'notes' => $userNotes, 'formNote' => $form->createView(),
        ]);
    }

    /**
     * @Route("/student/addFolder", name="add_folder_note")
     */
    public function addFolder(Request $request, ObjectManager $entityManager) {
        $folderName = $request->request->get('folder_name');
        if($folderName) {
            $currentUser= $this->getUser();
            if($currentUser) {
                //add a new folder for the connected user
                $folder = new StudentFolders();
                $folder->setName($folderName);
                $folder->setUser($currentUser);
                $entityManager->persist($folder);
                // actually executes the queries (i.e. the INSERT query)
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/student/updateFolder", name="update_folder")
     */
    public function updateFolderName(Request $request, ObjectManager $entityManager, StudentFoldersRepository $studentFolder) {

        $folderName = $request->request->get('folder_name');
        $folderId   = $request->request->get('id');
        $folder     = $studentFolder->find($folderId);
        if($folder) {
            $folder->setName($folderName);
            $entityManager->persist($folder);
            $entityManager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/student/deleteFolder", name="delete_folder")
     */
    public function deleteFolder(Request $request, ObjectManager $entityManager, StudentFoldersRepository $studentFolder) {
        $folderId   = $request->request->get('id');
        $folder     = $studentFolder->find($folderId);
        if($folder) {
            $entityManager->remove($folder);
            $entityManager->flush();
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/student/duplicateFolder", name="duplicate_folder")
     */
    public function duplicateFolder(Request $request, ObjectManager $entityManager) {
        $folderName = $request->request->get('folder_name');
        if($folderName) {
            $currentUser = $this->getUser();
            if($currentUser) {
                //add a new folder for the connected user
                $folder = new StudentFolders();
                $folder->setName($folderName.' copie');
                $folder->setUser($currentUser);
                $entityManager->persist($folder);
                // actually executes the queries (i.e. the INSERT query)
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        } else {
            return new Response(
                'error'
            );
        }
    }

    /**
     * @Route("/student/updateNote/{id}", name="update_note")
     */
    public function updateNote(ObjectManager $objectManger, Request $request, StudentNotes $note) {

        $form = $this->createFormBuilder($note)
            ->add('title')
            ->add('value', CKEditorType::class,['config' => ['uiColor' => '#ffffff', 'toolbar' => [['Undo','Redo','Bold','Italic','Underline','Strike', 'Blockquote','Subscript','Superscript','-',  'NumberedList','BulletedList','-','-','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock', 'Table', 'Styles', 'Format','Font','FontSize', 'TextColor' ], "/" ]]])
            ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            //update the date of the creation
            if(!$note->getId()) {
                $note->setCreatedAt(new \DateTime);
            }
            $objectManger->persist($note);
            $objectManger->flush();
            $this->addFlash(
                'success',
                'Votre note a été mise à jour!'
            );
            return $this->redirectToRoute('student_note');
        }
        return $this->render('notes_student/update_note.html.twig', [
            'formNote' => $form->createView(),
        ]);
    }

    /**
     * @Route("student/deleteNote/{id}", name="delete_note")
     */
    public function deleteNote(ObjectManager $entityManager, StudentNotes $note) {
        if($note) {
            $note->setStatus(0);
            $entityManager->persist($note);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Votre note a été suprimer avec succée!'
            );
            return $this->redirectToRoute('student_note');
        }
    }

    /**
     * @Route("student/showNotes/{id}", name="show_notes")
     */
    public function showNoteByFolder($id, StudentNotesRepository $studentNote) {
        //get all notes for the selected folder
        $userNotes = $studentNote->findBy(array('studentFolder' =>  $id ));
        return $this->render('notes_student/note_by_floder.html.twig', [
            'notes' => $userNotes
        ]);
    }

    /**
     * @Route("/student/duplicateNote/{id}", name="duplicate_note")
     */
    public function duplicateNote(ObjectManager $entityManager, StudentNotesRepository $studentNote, $id, StudentNotes $note) {

        $newNote = new StudentNotes();
        if($note) {
            $newNote->setCreatedAt(new \DateTime)
                    ->setValue($note->getValue())
                    ->setType($note->getType())
                    ->setStudentFolder($note->getStudentFolder())
                    ->setTitle($note->getTitle().' copie')
                    ->setStatus(1)
                    ->setUser($note->getUser());
                //add a new note
                $entityManager->persist($newNote);
                $entityManager->flush();

            $this->addFlash(
                'success',
                'Votre note a été dupliquée avec succée!'
            );

        }
        return $this->redirectToRoute('student_note');
    }

    /**
     * @Route("student/deleteNotes", name="delete_notes")
     */
    public function deleteNotes(ObjectManager $entityManager, Request $request, StudentNotesRepository $studentNote) {
        $notes = $request->request->get('notes');
      
        if(!empty($notes)) {
            foreach ($notes as $key => $note) {
                $currentNote = $studentNote->find($note);
                $currentNote->setStatus(0);
                $entityManager->persist($currentNote);
                $entityManager->flush();
            }
            return new Response(
                'success'
            );
        }
    
    }

    /**
     * @Route("/saveAudio", name="save_audio")
     */
    public function saveaudio() {
        return $this->render('notes_student/record.html.twig');
    } 
}
