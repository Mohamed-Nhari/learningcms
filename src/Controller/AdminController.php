<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Entity\Document;
use App\Entity\DocumentFolders;
use App\Repository\DocumentRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_global_menu")
     */
    public function index()
    {
    	return $this->render('admin/global_menu.html.twig');
    }

    /**
     * @Route("/admin/dashboardAdmin", name="admin_dashboard")
     */
    public function dashboardAdmin()
    {
        return $this->render('admin/dashboard_admin.html.twig');
    }

    /**
     * @Route("users/addRole/{id}", name="add_role")
     */
    public function addRole(User $user, ObjectManager $manager) {
        //add admin role
        if($user) {
			$user->addRole("ROLE_ADMIN");
        	$manager->persist($user);
        	$manager->flush();
        }
        return $this->redirectToRoute('home_page' );
    }
    /*public function getRoleName($role = nulle) {
        if($role == 'ROLE_USER' ) {
            return 'utilisateur';
        } else {
            return 'Administrateur';
        }
    }*/

     /**
     * @Route("/admin/getAllUser", name="get_list_users")
     */
    public function getAllUser()
    {
          return new JsonResponse(array('data' => 'nahed'));
    }
}
