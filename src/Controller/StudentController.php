<?php


namespace App\Controller;


use App\Entity\Abonnement;
use App\Entity\Preferences;
use App\Entity\StudentFolders;
use App\Entity\User;
use App\Form\PreferencesType;
use App\Form\ProfileType;
use App\Repository\PreferencesRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Response;

class StudentController extends AbstractController
{
    /**
     * @Route("/student/index", name="dashboard_student")
     */
    public function dashboard() {

        return $this->render('student/dashbaord.html.twig');
    }

    /**
     * @Route("/profile/{id}", name="user_profile")
     */
    public function edit_profile(ObjectManager $manager, Request $request, UserPasswordEncoderInterface $encoder, $id = null, UserRepository $userRepository) {

        if($id == null) {
            $user = $this->getUser();
            if(!$user) {
                return $this->redirectToRoute('login_form');
            }
        } else {
            $user = $userRepository->findOneBy(array('id' => $id));
            if(!$user) {
                return $this->redirectToRoute('users_list');
            }
        }

        $pass = $user->getPassword();
        $user->setUpdatedAt(new \DateTime);
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);
        $gender = $user->getgender();

        if($form->isSubmitted() && $form->isValid()) {
            $oldPassword = $request->request->get('profile')['password'];
            $gender = $request->request->get('gender');
            $user->setGender($gender);
            $user->setPassword($pass);
            if($oldPassword != "" ) {
               //modification de mot de passe
                if ($encoder->isPasswordValid($user, $oldPassword)) {
                    //if the same passord
                    $newEncodedPassword = $encoder->encodePassword($user, $user->getNewPassword());
                    $user->setPassword($newEncodedPassword);
                    $this->addFlash(
                        'success',
                        'Modification effectué avec succées!'
                    );

                } else  {
                    $this->addFlash('error', 'Ancien mot de passe incorrect !');
                }
            }

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('user_profile' );

        }

        return $this->render('user/user_profile.html.twig', [
            'formUser' => $form->createView(),'gender' =>  $gender
        ]);
    }

    /**
     * @Route("student/abonnement", name="student_abonnement")
     */
    public function abonnement() {
        //get all abonnements
        $repository = $this->getDoctrine()->getRepository(Abonnement::class);
        $abonnementsAnnuel  = $repository->findBy(['type' => 'annual']);
        $abonnementsMonthly = $repository->findBy(['type' => 'monthly']);

        return $this->render('student/abonnement.html.twig', [
            'annual' => $abonnementsAnnuel, 'monthly' => $abonnementsMonthly
        ]);
    }

    /**
     * @Route("student/pannier", name="student_pannier")
     */
    public function pannier() {
        return $this->render('student/pannier.html.twig');
    }

    /**
     * @Route("student/preferences", name="student_preferences")
     */
    public function preferences(ObjectManager $manager, Request $request, PreferencesRepository $preferencesRepository)
    {
        $pref = new Preferences();
        $form = $this->createForm(PreferencesType::class, $pref);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {


            $id = $this->getUser();

            $prefUser = $preferencesRepository->findOneBy(array('user' => $id));

            if ($request->isMethod('POST')) {

                $mailconf = $request->request->getInt('mail_conf');

                $vidconf = $request->request->getInt('vid_conf');

                if (!empty($prefUser)) {
                    //do update
                    $prefUser->setMailConfig($mailconf);

                    $prefUser->setVideoConfig($vidconf);

                    //$manager->persist($pref);

                    $manager->flush();

                    return $this->redirectToRoute('student_preferences');
                } else {
                    //do insert
                    $pref->setUser($id);


                    $pref->setMailConfig($mailconf);

                    $pref->setVideoConfig($vidconf);

                    $manager->persist($pref);

                    $manager->flush();

                    return $this->redirectToRoute('student_preferences');
                }
            }
        }
            return $this->render('student/preferences.html.twig',[
                'form' =>$form->createView()
            ]);
    }

    /**
     * @Route("student/products", name="student_products")
     */
    public function products() {
        return $this->render('student/products.html.twig');
    }

    /**
     * @Route("student/persenalData", name="student_data")
     */
    public function persenalData() {
        return $this->render('student/persenalData.html.twig');
    }

    /**
     * @Route("student/invoice", name="student_invoice")
     */
    public function invoice() {
        return $this->render('student/invoice.html.twig');
    }

    /**
     * @Route("/student/formations", name="student_formations")
     */
    public function formations() {
        return $this->render('student/formations.html.twig');
    }

     /**
     * @Route("/student/showFormations/{id}", name="show_formation")
     */
    public function showFormations() {

        return $this->render('student/showFormations.html.twig');
    }

    /**
    *@Route("student/showModule/{id}", name="show_module")
    */
    public function showModule() {
        return $this->render('student/showModule.html.twig');
    }
    /**
    *@Route("student/course/{id}", name="show_course")
    */
    public function course() {
        return $this->render('student/course.html.twig');
    }
}