<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfileType;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\Session;
class AuthenticationController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index() {
        $user  = $this->getUser();
        $roles = $user->getRoles();

        if($user->getActive() == 0) {
            //redirect to index page confirm_inscription
            return $this->redirectToRoute('confirm_inscription');
        }
        if(in_array('ROLE_ADMIN', $roles)) {
            //if user has role admin redirect him to dashboard admin
            return $this->redirectToRoute('admin_global_menu');
        } else if(in_array('ROLE_USER', $roles)) {
            //if user has role user redirect him to dashboard student
            return $this->redirectToRoute('dashboard_student');
        }
    }

    /**
     * @Route("/inscription", name="registration")
     */
    public function registration(ObjectManager $manager, Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        $user = new User();
        $user->setCreatedAT(new \DateTime)
            ->setUpdatedAt(new \DateTime)
            ->setActive(0);
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            //encoder le mot de passe
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);

            $manager->flush();

            //send confirmation mail
            $name = $user->getUsername();
            //update this link when the website is online
            $link = 'http://127.0.0.1:8000/login?email='.$user->getemail();
            $message = (new \Swift_Message('Confirmation d\'inscription!'))
                ->setFrom('nahed1606@hotmail.fr')
                ->setTo($user->getemail())
                ->setBody(
                    $this->renderView('emails/registration.html.twig',   ['name' => $name, 'link' => $link])  ,'text/html'
                ) ;
            $mailer->send($message);
            return $this->redirectToRoute('home_page');
        }

        return $this->render('authentication/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="login_form")
     */
    public function connexion(UserRepository $repo, ObjectManager $manager,AuthenticationUtils $authenticationUtils) {

        if(isset($_GET['email']) && !empty($_GET['email'])) {
            //when user open this link from the email we set active to 1 and the user have the chose to connect or not
            $email  = $_GET['email'];
            $user   = $repo
                ->findOneByEmail($email);
            if($user) {
                $user->setActive(1);
                $manager->persist($user);
                $manager->flush();
            }
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        //if the user is a student redirect him to the dashboard student
        return $this->render('authentication/login.html.twig',['error' => $error]);
    }

    /**
     * @Route("/logout", name="logout_form")
     */
    public function logout() {}

    /**
     * @Route("/confirmation", name="confirm_inscription")
     */
    public function confirmInscription() {
        return $this->render('authentication/confirm_inscription.html.twig');
    }

    /**
     * @Route("/users", name="list_users")
     */
    public function listUsers(UserRepository $repo) {
        $users = $repo->findAll();
        return $this->render('authentication/list_users.html.twig',['users' => $users]);
    }

    /**
     * @Route("users/{id}", name="profil_user")
     */
    public function show(User $user, ObjectManager $manager) {
        //add admin role
        /*$user->addRole("ROLE_ADMIN");
        $manager->persist($user);
        $manager->flush();
        */
        return $this->render('authentication/profil_users.html.twig',['user' => $user]);
    }

    /**
     * @Route("sendMail",name="sendMail")
     */
    public function sendMail(\Swift_Mailer $mailer) {
        /*$message = (new \Swift_Message('You Got Mail!'))
                       ->setFrom('nahed1606@hotmail.fr')
                       ->setTo('nahed1606@hotmail.fr')
                       ->setBody(
                           $this->renderView('emails/registration.html.twig',   ['name' =>'nahed'])  ,'text/html'
                       ) ;
         $mailer->send($message);

        */
    }

    /**
     * @Route("/profile2", name="edit_profile")
     */
    public function edit_profile(ObjectManager $manager, Request $request,UserPasswordEncoderInterface $encoder) {

        $user = $this->getUser();
        $user->setUpdatedAt(new \DateTime);
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);
        $userPassword = $user->getPassword();
        if($form->isSubmitted() && $form->isValid()) {
            $passwordEncoder = $this->get('security.password_encoder');
            dump($request->request);die();

        }

        return $this->render('student/user_profile.html.twig', [
            'formUser' => $form->createView()
        ]);
    }

    /**
     * @Route("/forgottenPassword", name="app_forgotten_password")
     */
    public function forgottenPassword(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator, ObjectManager $entityManager) {

        if ($request->isMethod('POST')) {
            $email = $request->request->get('email');
            $user = $entityManager->getRepository(User::class)->findOneByEmail($email);
            /* @var $user User */
            if ($user === null) {
                $this->addFlash('danger', 'Email Inconnu');
                return $this->redirectToRoute('app_forgotten_password');
            }
            $token = $tokenGenerator->generateToken();
            try{
                $user->setResetToken($token);
                $entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', $e->getMessage());
               return $this->redirectToRoute('app_forgotten_password');
            }
            $url = $this->generateUrl('app_reset_password', array('token' => $token), UrlGeneratorInterface::ABSOLUTE_URL);

            $message = (new \Swift_Message('Forgot Password'))
                ->setFrom('g.ponty@dev-web.io')
                ->setTo($user->getEmail())
                ->setBody(
                    "Voici le token pour reseter votre mot de passe : " . $url,
                    'text/html'
                );
            $mailer->send($message);
            $this->addFlash('notice', 'Mail envoyé');

            return $this->redirectToRoute('app_forgotten_password');
        }

        return $this->render('authentication/forget_password.html.twig');
    }

    /**
    * @Route("/reset_password/{token}", name="app_reset_password")
    */
    public function resetPassword(Request $request, string $token, UserPasswordEncoderInterface $passwordEncoder)
    {

        if ($request->isMethod('POST')) {
            $entityManager = $this->getDoctrine()->getManager();

            $user = $entityManager->getRepository(User::class)->findOneByResetToken($token);
            /* @var $user User */

            if ($user === null) {
                $this->addFlash('danger', 'Token Inconnu');
                return $this->redirectToRoute('login_form');
            }

            $user->setResetToken(null);
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $entityManager->flush();

            $this->addFlash('notice', 'Mot de passe mis à jour');

            return $this->redirectToRoute('login_form');
        }else {

            return $this->render('authentication/reset_password.html.twig', ['token' => $token]);
        }

    }

    /**
     *@Route("/deleteUser/{id}", name="delete_user")
     */
    public function deleteUser($id, ObjectManager $manager) {
        $currentUserId = $this->getUser()->getId();
        $repository = $this->getDoctrine()->getRepository(User::class);

        $user = $repository->find($id);
        if ($currentUserId == $id)
        {
            //delete the user session
            $session = $this->get('session');
            $session = new Session();
            $session->invalidate();
            //delete all user account from database
            if($user) {
                $manager->remove($user);
                $manager->flush();
            }
            return $this->redirectToRoute('login_form');

        } else {
            //only delete user action do by admin
            if($user) {
                $manager->remove($user);
                $manager->flush();
            }
            //return the admin to hear dashboard not exist yet
        }

    }

    /**
     * @Route("addRole/{id}", name="add_role")
     */
    public function addRole(User $user, ObjectManager $manager) {
        //add admin role
        $user->addRole("ROLE_ADMIN");
        $manager->persist($user);
        $manager->flush();

        //return $this->render('authentication/profil_users.html.twig',['user' => $user]);
    }
}