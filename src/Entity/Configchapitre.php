<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigchapitreRepository")
 */
class Configchapitre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $time_spent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status_completed;

    /**
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $score_point;

    /**
     * @ORM\Column(type="integer")
     */
    private $statut_accesss;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status_dateacces;

    /**
     * @ORM\Column(type="integer")
     */
    private $staut_subscribe;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $staut_datesubscribe;

    /**
     * @ORM\Column(type="integer")
     */
    private $staut_prerequis;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $choice_prerequis;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $valide_hours;

    /**
     * @ORM\Column(type="integer")
     */
    private $staut_chapitre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $choice_chaiptre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getTimeSpent(): ?string
    {
        return $this->time_spent;
    }

    public function setTimeSpent(string $time_spent): self
    {
        $this->time_spent = $time_spent;

        return $this;
    }

    public function getStatusCompleted(): ?string
    {
        return $this->status_completed;
    }

    public function setStatusCompleted(string $status_completed): self
    {
        $this->status_completed = $status_completed;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getScorePoint(): ?string
    {
        return $this->score_point;
    }

    public function setScorePoint(string $score_point): self
    {
        $this->score_point = $score_point;

        return $this;
    }

    public function getStatutAccesss(): ?int
    {
        return $this->statut_accesss;
    }

    public function setStatutAccesss(int $statut_accesss): self
    {
        $this->statut_accesss = $statut_accesss;

        return $this;
    }

    public function getStatusDateacces(): ?string
    {
        return $this->status_dateacces;
    }

    public function setStatusDateacces(string $status_dateacces): self
    {
        $this->status_dateacces = $status_dateacces;

        return $this;
    }

    public function getStautSubscribe(): ?int
    {
        return $this->staut_subscribe;
    }

    public function setStautSubscribe(int $staut_subscribe): self
    {
        $this->staut_subscribe = $staut_subscribe;

        return $this;
    }

    public function getStautDatesubscribe(): ?string
    {
        return $this->staut_datesubscribe;
    }

    public function setStautDatesubscribe(string $staut_datesubscribe): self
    {
        $this->staut_datesubscribe = $staut_datesubscribe;

        return $this;
    }

    public function getStautPrerequis(): ?int
    {
        return $this->staut_prerequis;
    }

    public function setStautPrerequis(int $staut_prerequis): self
    {
        $this->staut_prerequis = $staut_prerequis;

        return $this;
    }

    public function getChoicePrerequis(): ?string
    {
        return $this->choice_prerequis;
    }

    public function setChoicePrerequis(string $choice_prerequis): self
    {
        $this->choice_prerequis = $choice_prerequis;

        return $this;
    }

    public function getValideHours(): ?string
    {
        return $this->valide_hours;
    }

    public function setValideHours(string $valide_hours): self
    {
        $this->valide_hours = $valide_hours;

        return $this;
    }

    public function getStautChapitre(): ?int
    {
        return $this->staut_chapitre;
    }

    public function setStautChapitre(int $staut_chapitre): self
    {
        $this->staut_chapitre = $staut_chapitre;

        return $this;
    }

    public function getChoiceChaiptre(): ?string
    {
        return $this->choice_chaiptre;
    }

    public function setChoiceChaiptre(string $choice_chaiptre): self
    {
        $this->choice_chaiptre = $choice_chaiptre;

        return $this;
    }
}
