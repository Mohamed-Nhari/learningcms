<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChaptersRepository")
 */
class Chapters
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;



    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Quiz", cascade={"persist", "remove"})
     */
    private $quiz;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cours", inversedBy="chaiptres")
     */
    private $cours;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="chaiptres")
     */
    private $formation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Configchapitre", cascade={"persist", "remove"})
     */
    private $config;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BuilderContent", mappedBy="chapter", cascade={"persist", "remove"})
     */
    private $builderContent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

/*    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }*/

/*    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }*/



    public function getQuiz(): ?Quiz
    {
        return $this->quiz;
    }

    public function setQuiz(?Quiz $quiz): self
    {
        $this->quiz = $quiz;

        return $this;
    }

    public function getCours(): ?Cours
    {
        return $this->cours;
    }

    public function setCours(?Cours $cours): self
    {
        $this->cours = $cours;

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getConfig(): ?Configchapitre
    {
        return $this->config;
    }

    public function setConfig(?Configchapitre $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function getBuilderContent(): ?BuilderContent
    {
        return $this->builderContent;
    }

    public function setBuilderContent(BuilderContent $builderContent): self
    {
        $this->builderContent = $builderContent;

        // set the owning side of the relation if necessary
        if ($this !== $builderContent->getChapter()) {
            $builderContent->setChapter($this);
        }

        return $this;
    }
}
