<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConfigcoursRepository")
 */
class Configcours
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer" , nullable=true)
     */
    private $score;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $score_point;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statut_access;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $date_access;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $access_subscribe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $day_acces;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statuts_prerequis;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $select_choiceprerequis;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numberpoints_prerequis;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getScorePoint(): ?string
    {
        return $this->score_point;
    }

    public function setScorePoint(string $score_point): self
    {
        $this->score_point = $score_point;

        return $this;
    }

    public function getStatutAccess(): ?int
    {
        return $this->statut_access;
    }

    public function setStatutAccess(int $statut_access): self
    {
        $this->statut_access = $statut_access;

        return $this;
    }

    public function getDateAccess(): ?string
    {
        return $this->date_access;
    }

    public function setDateAccess(string $date_access): self
    {
        $this->date_access = $date_access;

        return $this;
    }

    public function getAccessSubscribe(): ?int
    {
        return $this->access_subscribe;
    }

    public function setAccessSubscribe(int $access_subscribe): self
    {
        $this->access_subscribe = $access_subscribe;

        return $this;
    }

    public function getDayAcces(): ?string
    {
        return $this->day_acces;
    }

    public function setDayAcces(string $day_acces): self
    {
        $this->day_acces = $day_acces;

        return $this;
    }

    public function getStatutsPrerequis(): ?int
    {
        return $this->statuts_prerequis;
    }

    public function setStatutsPrerequis(int $statuts_prerequis): self
    {
        $this->statuts_prerequis = $statuts_prerequis;

        return $this;
    }

    public function getSelectChoiceprerequis(): ?string
    {
        return $this->select_choiceprerequis;
    }

    public function setSelectChoiceprerequis(string $select_choiceprerequis): self
    {
        $this->select_choiceprerequis = $select_choiceprerequis;

        return $this;
    }

    public function getNumberpointsPrerequis(): ?string
    {
        return $this->numberpoints_prerequis;
    }

    public function setNumberpointsPrerequis(string $numberpoints_prerequis): self
    {
        $this->numberpoints_prerequis = $numberpoints_prerequis;

        return $this;
    }
}
