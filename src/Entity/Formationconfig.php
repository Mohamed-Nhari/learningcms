<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationconfigRepository")
 */
class Formationconfig
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $preq_formation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $point_prequis;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $point_note;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $list_note;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hour_time;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $list_temp;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enable_comment;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enable_point;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enable_hours;

        /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $disable_quiz;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enable_hourbyday;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enable_inscrit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $insert_forform;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $form_member;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $chaine_formation;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enable_description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enable_autodesccomplete;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enanble_invit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
private  $progress_enableinvit;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language): void
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getPreqFormation()
    {
        return $this->preq_formation;
    }

    /**
     * @param mixed $preq_formation
     */
    public function setPreqFormation($preq_formation): void
    {
        $this->preq_formation = $preq_formation;
    }

    /**
     * @return mixed
     */
    public function getPointPrequis()
    {
        return $this->point_prequis;
    }

    /**
     * @param mixed $point_prequis
     */
    public function setPointPrequis($point_prequis): void
    {
        $this->point_prequis = $point_prequis;
    }

    /**
     * @return mixed
     */
    public function getPointNote()
    {
        return $this->point_note;
    }

    /**
     * @param mixed $point_note
     */
    public function setPointNote($point_note): void
    {
        $this->point_note = $point_note;
    }

    /**
     * @return mixed
     */
    public function getListNote()
    {
        return $this->list_note;
    }

    /**
     * @param mixed $list_note
     */
    public function setListNote($list_note): void
    {
        $this->list_note = $list_note;
    }

    /**
     * @return mixed
     */
    public function getHourTime()
    {
        return $this->hour_time;
    }

    /**
     * @param mixed $hour_time
     */
    public function setHourTime($hour_time): void
    {
        $this->hour_time = $hour_time;
    }

    /**
     * @return mixed
     */
    public function getListTemp()
    {
        return $this->list_temp;
    }

    /**
     * @param mixed $list_temp
     */
    public function setListTemp($list_temp): void
    {
        $this->list_temp = $list_temp;
    }

    /**
     * @return mixed
     */
    public function getEnableComment()
    {
        return $this->enable_comment;
    }

    /**
     * @param mixed $enable_comment
     */
    public function setEnableComment($enable_comment): void
    {
        $this->enable_comment = $enable_comment;
    }

    /**
     * @return mixed
     */
    public function getEnablePoint()
    {
        return $this->enable_point;
    }

    /**
     * @param mixed $enable_point
     */
    public function setEnablePoint($enable_point): void
    {
        $this->enable_point = $enable_point;
    }

    /**
     * @return mixed
     */
    public function getEnableHours()
    {
        return $this->enable_hours;
    }

    /**
     * @param mixed $enable_hours
     */
    public function setEnableHours($enable_hours): void
    {
        $this->enable_hours = $enable_hours;
    }

    /**
     * @return mixed
     */
    public function getEnableHourbyday()
    {
        return $this->enable_hourbyday;
    }

    /**
     * @param mixed $enable_hourbyday
     */
    public function setEnableHourbyday($enable_hourbyday): void
    {
        $this->enable_hourbyday = $enable_hourbyday;
    }

    /**
     * @return mixed
     */
    public function getEnableInscrit()
    {
        return $this->enable_inscrit;
    }

    /**
     * @param mixed $enable_inscrit
     */
    public function setEnableInscrit($enable_inscrit): void
    {
        $this->enable_inscrit = $enable_inscrit;
    }

    /**
     * @return mixed
     */
    public function getInsertForform()
    {
        return $this->insert_forform;
    }

    /**
     * @param mixed $insert_forform
     */
    public function setInsertForform($insert_forform): void
    {
        $this->insert_forform = $insert_forform;
    }

    /**
     * @return mixed
     */
    public function getFormMember()
    {
        return $this->form_member;
    }

    /**
     * @param mixed $form_member
     */
    public function setFormMember($form_member): void
    {
        $this->form_member = $form_member;
    }

    /**
     * @return mixed
     */
    public function getChaineFormation()
    {
        return $this->chaine_formation;
    }

    /**
     * @param mixed $chaine_formation
     */
    public function setChaineFormation($chaine_formation): void
    {
        $this->chaine_formation = $chaine_formation;
    }

    /**
     * @return mixed
     */
    public function getEnableDescription()
    {
        return $this->enable_description;
    }

    /**
     * @param mixed $enable_description
     */
    public function setEnableDescription($enable_description): void
    {
        $this->enable_description = $enable_description;
    }

    /**
     * @return mixed
     */
    public function getEnableAutodesccomplete()
    {
        return $this->enable_autodesccomplete;
    }

    /**
     * @param mixed $enable_autodesccomplete
     */
    public function setEnableAutodesccomplete($enable_autodesccomplete): void
    {
        $this->enable_autodesccomplete = $enable_autodesccomplete;
    }

    /**
     * @return mixed
     */
    public function getEnanbleInvit()
    {
        return $this->enanble_invit;
    }

    /**
     * @param mixed $enanble_invit
     */
    public function setEnanbleInvit($enanble_invit): void
    {
        $this->enanble_invit = $enanble_invit;
    }

    /**
     * @return mixed
     */
    public function getProgressEnableinvit()
    {
        return $this->progress_enableinvit;
    }

    /**
     * @param mixed $progress_enableinvit
     */
    public function setProgressEnableinvit($progress_enableinvit): void
    {
        $this->progress_enableinvit = $progress_enableinvit;
    }

    /**
     * @return mixed
     */
    public function getDisableQuiz()
    {
        return $this->disable_quiz;
    }

    /**
     * @param mixed $disable_quiz
     */
    public function setDisableQuiz($disable_quiz): void
    {
        $this->disable_quiz = $disable_quiz;
    }



}
