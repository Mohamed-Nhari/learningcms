<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BadgeRepository")
 * @Vich\Uploadable
 */
class Badge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @Vich\UploadableField(mapping="badges_images", fileNameProperty="picture_name")
     * @var File
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $attribute;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $valid;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $period;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $duty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quiz;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $score;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $time;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $forum;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $rewards;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $val_duty;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $val_valid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $val_quiz;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $val_time;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $certifs;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $note_val;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $picture_name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime
     */
    private $updateAt;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getPicture(): ?File
    {
        return $this->picture;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $picture_name
     * @throws \Exception
     */
    public function setPicture(?File $picture_name = null):void
    {
        $this->picture = $picture_name;
        if($picture_name){
            $this->updatedAt = new \DateTime('now');
        }
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAttribute(): ?int
    {
        return $this->attribute;
    }

    public function setAttribute(?int $attribute): self
    {
        $this->attribute = $attribute;

        return $this;
    }


    public function getValid(): ?int
    {
        return $this->valid;
    }

    public function setValid(?int $valid): self
    {
        $this->valid = $valid;

        return $this;
    }

    public function getPeriod(): ?int
    {
        return $this->period;
    }

    public function setPeriod(?int $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getDuty(): ?string
    {
        return $this->duty;
    }

    public function setDuty(?string $duty): self
    {
        $this->duty = $duty;

        return $this;
    }

    public function getQuiz(): ?string
    {
        return $this->quiz;
    }

    public function setQuiz(?string $quiz): self
    {
        $this->quiz = $quiz;

        return $this;
    }

    public function getScore(): ?string
    {
        return $this->score;
    }

    public function setScore(?string $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(?string $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getForum(): ?string
    {
        return $this->forum;
    }

    public function setForum(?string $forum): self
    {
        $this->forum = $forum;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRewards()
    {
        return $this->rewards;
    }

    /**
     * @param mixed $rewards
     */
    public function setRewards($rewards): void
    {
        $this->rewards = $rewards;
    }



    public function getValDuty(): ?string
    {
        return $this->val_duty;
    }

    public function setValDuty(?string $val_duty): self
    {
        $this->val_duty = $val_duty;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValValid()
    {
        return $this->val_valid;
    }

    /**
     * @param mixed $val_valid
     */
    public function setValValid($val_valid): void
    {
        $this->val_valid = $val_valid;
    }

    public function getValQuiz(): ?string
    {
        return $this->val_quiz;
    }

    public function setValQuiz(?string $val_quiz): self
    {
        $this->val_quiz = $val_quiz;

        return $this;
    }

    public function getValTime(): ?string
    {
        return $this->val_time;
    }

    public function setValTime(?string $val_time): self
    {
        $this->val_time = $val_time;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCertifs()
    {
        return $this->certifs;
    }

    /**
     * @param mixed $certifs
     */
    public function setCertifs($certifs): void
    {
        $this->certifs = $certifs;
    }



    public function getNoteVal(): ?string
    {
        return $this->note_val;
    }

    public function setNoteVal(?string $note_val): self
    {
        $this->note_val = $note_val;

        return $this;
    }

    public function getPictureName(): ?string
    {
        return $this->picture_name;
    }

    public function setPictureName(?string $picture_name): self
    {
        $this->picture_name = $picture_name;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }


}
