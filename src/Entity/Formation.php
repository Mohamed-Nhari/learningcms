<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $titre_formation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descprition_formation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sens_formation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_formation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $video_formation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $confignote_formation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nodetype_formation;

    /**
     * @return mixed
     */
    public function getNodetypeFormation()
    {
        return $this->nodetype_formation;
    }

    /**
     * @param mixed $nodetype_formation
     */
    public function setNodetypeFormation($nodetype_formation): void
    {
        $this->nodetype_formation = $nodetype_formation;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="formations")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notemax_formation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Formationconfig", cascade={"persist", "remove"})
     */
    private $configformation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Module", mappedBy="formation")
     */
    private $modules;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="formations")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Catalogue", inversedBy="formations")
     */
    private $catalogue;



    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quiz", mappedBy="formation")
     */
    private $quizzes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Chapters", mappedBy="formation")
     */
    private $chapters;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cours", mappedBy="formation")
     */
    private $cours;

    public function __construct()
    {
        $this->modules = new ArrayCollection();

   
        $this->quizzes = new ArrayCollection();
        $this->chapters = new ArrayCollection();
        $this->cours = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getConfigformation()
    {
        return $this->configformation;
    }

    /**
     * @param mixed $configformation
     */
    public function setConfigformation($configformation): void
    {
        $this->configformation = $configformation;
    }





    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitreFormation(): ?string
    {
        return $this->titre_formation;
    }

    public function setTitreFormation(string $titre_formation): self
    {
        $this->titre_formation = $titre_formation;

        return $this;
    }

    public function getDescpritionFormation(): ?string
    {
        return $this->descprition_formation;
    }

    public function setDescpritionFormation(string $descprition_formation): self
    {
        $this->descprition_formation = $descprition_formation;

        return $this;
    }

    public function getSensFormation(): ?int
    {
        return $this->sens_formation;
    }

    public function setSensFormation(int $sens_formation): self
    {
        $this->sens_formation = $sens_formation;

        return $this;
    }

    public function getImageFormation(): ?string
    {
        return $this->image_formation;
    }

    public function setImageFormation(string $image_formation): self
    {
        $this->image_formation = $image_formation;

        return $this;
    }

    public function getVideoFormation(): ?string
    {
        return $this->video_formation;
    }

    public function setVideoFormation(string $video_formation): self
    {
        $this->video_formation = $video_formation;

        return $this;
    }

    public function getConfignoteFormation(): ?string
    {
        return $this->confignote_formation;
    }

    public function setConfignoteFormation(string $confignote_formation): self
    {
        $this->confignote_formation = $confignote_formation;

        return $this;
    }



    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getNotemaxFormation(): ?string
    {
        return $this->notemax_formation;
    }

    public function setNotemaxFormation(string $notemax_formation): self
    {
        $this->notemax_formation = $notemax_formation;

        return $this;
    }

    /**
     * @return Collection|Module[]
     */
    public function getModules(): Collection
    {
        return $this->modules;
    }

    public function addModule(Module $module): self
    {
        if (!$this->modules->contains($module)) {
            $this->modules[] = $module;
            $module->setFormation($this);
        }

        return $this;
    }

    public function removeModule(Module $module): self
    {
        if ($this->modules->contains($module)) {
            $this->modules->removeElement($module);
            // set the owning side to null (unless already changed)
            if ($module->getFormation() === $this) {
                $module->setFormation(null);
            }
        }

        return $this;
    }





    /**
     * @return Collection|Quiz[]
     */
    public function getQuizzes(): Collection
    {
        return $this->quizzes;
    }

    public function addQuiz(Quiz $quiz): self
    {
        if (!$this->quizzes->contains($quiz)) {
            $this->quizzes[] = $quiz;
            $quiz->setFormation($this);
        }

        return $this;
    }

    public function removeQuiz(Quiz $quiz): self
    {
        if ($this->quizzes->contains($quiz)) {
            $this->quizzes->removeElement($quiz);
            // set the owning side to null (unless already changed)
            if ($quiz->getFormation() === $this) {
                $quiz->setFormation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Chapters[]
     */
    public function getChapters(): Collection
    {
        return $this->chapters;
    }

    public function addChapters(Chapters $chapters): self
    {
        if (!$this->chapters->contains($chapters)) {
            $this->chapters[] = $chapters;
            $chapters->setFormation($this);
        }

        return $this;
    }

    public function removeChapters(Chapters $chapters): self
    {
        if ($this->chapters->contains($chapters)) {
            $this->chapters->removeElement($chapters);
            // set the owning side to null (unless already changed)
            if ($chapters->getFormation() === $this) {
                $chapters->setFormation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Cours $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
            $cour->setFormation($this);
        }

        return $this;
    }

    public function removeCour(Cours $cour): self
    {
        if ($this->cours->contains($cour)) {
            $this->cours->removeElement($cour);
            // set the owning side to null (unless already changed)
            if ($cour->getFormation() === $this) {
                $cour->setFormation(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCatalogue()
    {
        return $this->catalogue;
    }

    /**
     * @param mixed $catalogue
     */
    public function setCatalogue($catalogue): void
    {
        $this->catalogue = $catalogue;
    }



    


}
