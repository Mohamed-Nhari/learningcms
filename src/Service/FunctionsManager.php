<?php
namespace App\Service;

use App\Entity\Code;
use App\Repository\CodeRepository;

;

class FunctionsManager
{

    /**
     * @var CodeRepository
     */
    private $codeRepository;

    /** this funcion return the full name of the user role according to the user role saved on database */

    public function get_roles($role = 'ROLE_USER') {
        if($role == 'ROLE_ADMIN') {
            return 'Administrateur';
        } elseif($role == 'ROLE_USER') {
            return 'Étudiant';
        } elseif($role == 'ROLE_EDITOR') {
            return 'Editeur';
        }elseif($role == 'ROLE_COACH') {
            return 'Coach';
        } elseif($role == 'ROLE_FORMER ') {
            return 'formateur';
        } else {
            return 'Étudiant';
        }
    }

    public function getFormations($forms):array {
        $formation = $this->codeRepository->findBy(array('formation' =>  $forms ));
        $formations = $this->codeRepository->findBy(['formation' => $formation]);
        return implode(', ', $formations);
    }
}