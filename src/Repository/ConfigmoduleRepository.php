<?php

namespace App\Repository;

use App\Entity\Configmodule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Configmodule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Configmodule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Configmodule[]    findAll()
 * @method Configmodule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfigmoduleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Configmodule::class);
    }

    // /**
    //  * @return Configmodule[] Returns an array of Configmodule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Configmodule
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
