<?php

namespace App\Repository;

use App\Entity\Configchapitre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Configchapitre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Configchapitre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Configchapitre[]    findAll()
 * @method Configchapitre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfigchapitreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Configchapitre::class);
    }

    // /**
    //  * @return Configchapitre[] Returns an array of Configchapitre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Configchapitre
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
