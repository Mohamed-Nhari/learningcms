var to_send ={};
var module = {};
var data = [];
var nameofmodule={};
var moduleconfig_forms =  [];
    function functioncharteformation() {

        var currentNode = '';
        var clientXY = [0,0];
        var nameformation_recup= document.getElementById('getnameformation').value;
        OrgChart.templates.ula.size = [150, 55];
        OrgChart.templates.ula.pointer = '<g data-pointer="pointer"></g>';
        OrgChart.templates.ula.node = '<rect x="0" y="0" height="55" width="150" fill="#039BE5" stroke-width="1" stroke="#aeaeae" rx="5" ry="5"></rect>';
        OrgChart.templates.ula.field_0 = '<text width="230" class="field_0"  style="font-size: 18px;" fill="#ffffff" x="75" y="35" text-anchor="middle">{val}</text>';
        OrgChart.templates.ula.edit = '<g data-edit-id="{val}" transform="matrix(1,0,0,1,20,10)"><rect x="0" y="0" width="16" height="16" fill="#039BE5"></rect>' + OrgChart.icon.edit(16,16, '#fff') + '</g>';
        OrgChart.templates.ula.remove = '<g data-remove-id="{val}" transform="matrix(1,0,0,1,30,6)"><rect x="0" y="0" width="16" height="16" fill="#039BE5"></rect>' + OrgChart.icon.remove(15,15, '#fff') + '</g>';
        OrgChart.templates.ula.pdf = '<g data-pdf-id="{val}" transform="matrix(1,0,0,1,5,3)"><rect x="0" y="0" width="20" height="20" fill="#039BE5"></rect>' +OrgChart.icon.pdf(24,24, '#fff') +  '</g>';
        var chart = new OrgChart(document.getElementById("tree"), {
            template: "ula",
            enableSearch: false,
            mouseScrool: OrgChart.action.none,
            align: OrgChart.ORIENTATION,
            enableDragDrop: true,
            nodeMouseClick: OrgChart.action.none,
            nodeBinding: {
                field_0: "name",
                edit: 'id',
                remove: 'id',
                pdf:'id'
            },
            tags: {
                "assistant": {
                    template: "ula"
                }
            },
            toolbar:{
                zoom:true
            },

        });



        chart.on('redraw', function(){
            var editBtns = document.querySelectorAll('[data-edit-id]');

            for(var i = 0; i < editBtns.length; i++){
                editBtns[i].addEventListener('click',  function(){
                    chart.editUI.show(this.getAttribute("data-edit-id"));
                });
            }
            var removeBtns = document.querySelectorAll('[data-remove-id]');
            for(var i = 0; i < removeBtns.length; i++){
                removeBtns[i].addEventListener('click',  function(){
                    chart.removeNode(this.getAttribute("data-remove-id"));
                });

            }

            var butonpopup = document.querySelectorAll('[data-pdf-id]');

            for(var i = 0; i < butonpopup.length; i++){
                butonpopup[i].addEventListener('click',  function(){

                    console.log($(this).attr('data-pdf-id'));
                    var substrid=$(this).attr('data-pdf-id');
                    substrid = substrid.replace(/[0-9]/g, '');
                   // $("#moduleconfig"+$(this).attr('data-pdf-id')).modal('show');
                    if(substrid=="itemdat-"){

                        $("#moduleconfigitemdat").modal('show');
                        $("#valueofdat").val($(this).attr('data-pdf-id'));
                        $("#chaiptreconfig").modal('hide');
                        $("#courspopup").modal('hide');
                    }
                    if(substrid=="itemcour-"){
                        $("#courspopup").modal('show');
                        $("#valueofdat").val($(this).attr('data-pdf-id'));
                        $("#moduleconfigitemdat").modal('hide');
                        $("#chaiptreconfig").modal('hide');
                    }
                    if(substrid=="itemshap-"){
                        $("#chaiptreconfig").modal('show');
                        $("#moduleconfigitemdat").modal('hide');
                        $("#valueofdat").val($(this).attr('data-pdf-id'));
                        $("#courspopup").modal('hide');
                    }
                });
            }
            var nodeElements = document.querySelectorAll('[node-id]');
            var idsElements = [];
            for(var i = 0; i < nodeElements.length; i++){
                var nodeElement = nodeElements[i];
                nodeElement.ondrop = function(ev) {
                    ev.preventDefault();
                    var id = ev.dataTransfer.getData("id");

                    idsElements.push(id);
                    var itemss = document.querySelector('[data-id="' + id + '"]');
                    var name = itemss.innerText;
                    var addclass=itemss.setAttribute("data-id", ""+id + "01"+ "");
                    var module_popup = $("#moduleconfig").clone();
                    $("#moduleconfig").after($(module_popup).attr('id', "moduleconfig"+id+"01"));
                    itemss.querySelector("input").value++;
                    var x = document.getElementsByClassName('blocgloab');
                    itemss.querySelector("input").parentElement.parentElement.classList.add("classadd");
                    var nodeElement = ev.target;
                    while(!nodeElement.hasAttribute('node-id')){
                        nodeElement = nodeElement.parentNode;
                    }
                    var pid = nodeElement.getAttribute('node-id');
                    module.data = data;
                    var namodule = name;
                    var str_name = name;
                    str_name = str_name.replace(/[0-9]/g, '');
                   nameofmodule = {
                        "id": id,
                        "name": namodule,
                        "pid": pid,
                       "type":str_name
                    }
                    module.data.push(nameofmodule);
                    var bottoms = []

                    data.forEach(r => {
                        if (!data.find(p => r.id == p.pid))
                           bottoms.push(r);

                    })
                    moduleconfig_forms.push(nameofmodule);

                    bottoms.forEach(getParentNodes)

                    function getParentNodes(r) {
                        var p = r.parentNodes = []
                        while (r = data.find(a => a.id === r.pid)) {
                            p.push([r.name])

                           // moduleconfig_forms.config.push(r.name);
                        }

                    }

                    to_send=JSON.stringify(bottoms, 4, 4);

                    chart.addNode({
                        id: id,
                        pid: pid,
                        name: name
                    }, true);


                }

                nodeElement.ondragover = function(ev) {
                    ev.preventDefault();
                }
            }
        });

        chart.load([
            {id: 1, name: nameformation_recup},

        ]);

        var items = document.querySelectorAll('.item');
        for(var i = 0; i < items.length; i++){
            var item = items[i];
            item.draggable = true;
            item.ondragstart = function(ev) {
                ev.dataTransfer.setData("id", ev.target.getAttribute('data-id'));
            }
        }


    }

    window.onload = function() {
        if (document.getElementById("add-config")) {
            document.getElementById("add-config").onclick = function fun() {
                functioncharteformation();
            }
        }
    }

    $( document ).on( "click", ".delete, .delete1 ,.delete2", function() {


            $(this).parent('div').parent('div').remove();


        });
    $(document).on('click', '.edit, .edit1 , .edit2', function() {
        $(this).closest('.blocimage').next('.item').prop('contenteditable', true).focus();
    });

    var comptei=100;
    var compteurtextclone=1;
    $(document).on('click', '.cloner', function() {

        var $clone = $(this).closest('#collapseModule .blocgloab').clone();
        var getid = $(this).closest('#collapseModule .blocimage').next('.item').attr("id");
        var num = $(this).closest('#collapseModule .blocimage').next('.item').attr("data-id");

        getid=getid+comptei;
        num = num+comptei;

        $clone.insertAfter('#collapseModule .blocgloab:last');
        var textedit=$('#collapseModule .item:last').text();
        var totaltext=textedit+' -'+" copy"+compteurtextclone;
        // $('#collapseModule .item:last').empty();
        $('#collapseModule .item:last').attr('data-id', num);
        $('#collapseModule .item:last').attr('id', getid);
        $('#collapseModule .item:last').text(totaltext);
        $('#collapseModule .item:last').append('<input type="number" value="0">');
        comptei++;
        compteurtextclone++;
        functioncharteformation();
    });
    $(document).on('click', '.cloner1', function() {

        var $clone = $(this).closest('#list-cour .blocgloab').clone();
        var getid = $(this).closest('#list-cour .blocimage').next('.item').attr("id");
        var num = $(this).closest('#list-cour .blocimage').next('.item').attr("data-id");

        getid=getid+comptei;
        num = num+comptei;
        $clone.insertAfter('#list-cour .blocgloab:last');
        var textedit=$('#list-cour .item:last').text();
        var totaltext=textedit+' -'+" copy"+compteurtextclone;
        // $('#collapseModule .item:last').empty();
        $('#list-cour .item:last').attr('data-id', num);
        $('#list-cour .item:last').attr('id', getid);
        $('#list-cour .item:last').text(totaltext);
        $('#list-cour .item:last').append('<input type="number" value="0">');
        comptei++;
        compteurtextclone++;
        functioncharteformation();
    });
    $(document).on('click', '.cloner2', function() {

        var $clone = $(this).closest('#list-chap .blocgloab').clone();
        var getid = $(this).closest('#list-chap .blocimage').next('.item').attr("id");
        var num = $(this).closest('#list-chap .blocimage').next('.item').attr("data-id");

        getid=getid+comptei;
        num = num+comptei;

        $clone.insertAfter('#list-chap .blocgloab:last');
        var textedit=$('#list-chap .item:last').text();
        var totaltext=textedit+' -'+" copy"+compteurtextclone;
        // $('#collapseModule .item:last').empty();
        $('#list-chap .item:last').attr('data-id', num);
        $('#list-chap .item:last').attr('id', getid);
        $('#list-chap .item:last').text(totaltext);
        $('#list-chap .item:last').append('<input type="number" value="0">');
        comptei++;
        compteurtextclone++;
        functioncharteformation();
    });
    $(document).on('click', '.cloner3', function() {

        var $clone = $(this).closest('#list-quiz .blocgloab').clone();
        var getid = $(this).closest('#list-quiz .blocimage').next('.item').attr("id");
        var num = $(this).closest('#list-quiz .blocimage').next('.item').attr("data-id");

        getid=getid+comptei;
        num = num+comptei;
        $clone.insertAfter('#list-quiz .blocgloab:last');
        var textedit=$('#list-quiz .item:last').text();
        var totaltext=textedit+' -'+" copy"+compteurtextclone;
        // $('#collapseModule .item:last').empty();
        $('#list-quiz .item:last').attr('data-id', num);
        $('#list-quiz .item:last').attr('id', getid);
        $('#list-quiz .item:last').text(totaltext);
        $('#list-quiz .item:last').append('<input type="number" value="0">');
        comptei++;
        compteurtextclone++;
        functioncharteformation();
    });

$(document).on('click', "#btnenvoyemodule",function(e){
    $( "div[id^='moduleconfigitemdat']" ).each(function(){
        var getidmodulefrompup=$("#valueofdat").val();
        var statut_score = 0;
        var access=0;
        var access_subscribe=0;
        var icone;
        var statut_prequis;
        $(this).find('input').each(function(){
            if($(this).attr('type') == 'checkbox'){
                if($(this).is(':checked')){
                 statut_score = 1;
                    access=1;
                    access_subscribe=1;
                    statut_prequis=1;
                }
            }
        });
        var score = $('#score').val();
        var access_input=$("#access_input").val();
        var point_preq=$("#point_preq").val();
        var icone_image=$("#icone_image").val();
        var icones= $("#icon").val();
        var choice_preq=$("#choice_preq").val();
        moduleconfig_forms.forEach(function(obj) {
            if(obj.id == getidmodulefrompup) {
                //update the component file
                obj.score = score;
                obj.statut_score = statut_score;
                obj.access=access;
                obj.access_input=access_input;
                obj.access_subscribe=access_subscribe;
                obj.statut_prequis=statut_prequis;
                obj.point_req=point_preq;
                obj.icone_image=icone_image;
                obj.icone=icones;
                obj.choice_preq=choice_preq;
            }
        });
        $("#btnenvoyemodule").modal('hide');


    });
});


// Cours

$(document).on('click', "#btnenvoyemodulecours",function(e){

    $( "div[id^='courspopup']" ).each(function(){
        var getidmodulefrompup=$("#valueofdat").val();
        var score = 0;
        var statut_access=0;
        var access_subscribe=0;
        var icone;
        var statuts_prerequis=0;
        $(this).find('input').each(function(){
            if($(this).attr('type') == 'checkbox'){
                if($(this).is(':checked')){
                    score = 1;
                    statut_access=1;
                    access_subscribe=1;
                    statuts_prerequis=1;
                }
            }
        });
        var score_point=$("#score_point").val();
        var date_access=$("#date_access").val();
        var acces_days=$("#acces_days").val();
        var day_acces=$("#day_acces").val();
        var numberpoints_prerequis=$("#numberpoints_prerequis").val();
        var select_choiceprerequis=$("#select_choiceprerequis").val();
        moduleconfig_forms.forEach(function(obj) {
            if(obj.id == getidmodulefrompup) {
                //update the component file
                obj.score = score;
                obj.statut_access=	statut_access;
                obj.score_point=score_point;
                obj.date_access=date_access;
                obj.day_acces=day_acces;
                obj.statuts_prerequis=statuts_prerequis;
                obj.access_subscribe=access_subscribe;
             obj.numberpoint_prerequis=numberpoints_prerequis;
                obj.select_choiceprerequis=select_choiceprerequis;
            }
        });
        $("#btnenvoyemodulecours").modal('hide');


    });
});
//chaiptre

$(document).on('click', "#btnenvoyemodulecours",function(e){

    $( "div[id^='chaiptreconfig']" ).each(function(){
var status_completed=0;
var score=0;
var statut_accesss=0;
var staut_subscribe=0;
var staut_prerequis=0;
        $(this).find('input').each(function(){
            if($(this).attr('type') == 'checkbox'){
                if($(this).is(':checked')){
                    status_completed=1;
                    score=1;
                    statut_accesss=1;
                    staut_subscribe=1;
                    staut_prerequis=1;
                }
            }
        });
 var picture=$("#picture").val();
 var time_spent=$("#time_spent").val();
 var score_point=$("#score_point").val();
 var status_dateacces=$("#status_dateacces").val();
 var staut_datesubscribe=$("#staut_datesubscribe").val();
        moduleconfig_forms.forEach(function(obj) {
            if(obj.id == getidmodulefrompup) {
                //update the component file
 obj.picture=picture;
 obj.time_spent=time_spent;
 obj.status_completed=status_completed;
                obj.score=score;
                obj.score_point=score_point;
                obj.statut_accesss=statut_accesss;
                obj.status_dateacces=status_dateacces;
                obj.staut_subscribe=staut_subscribe;
                obj.staut_datesubscribe=staut_datesubscribe;
                obj.staut_prerequis=staut_prerequis;

            }
        });
        $("#btnenvoyemodulechaiptre").modal('hide');


    });
});
$(document).on('click',".monthly-btn",function (e) {
    console.log("hello");
    e.preventDefault();
    var element = $(this);
    console.log(to_send);
    if(typeof url_to_send_to !== "undefined") {

        $.ajax({
            type: "POST",
            url: url_to_send_to,
            data: { url_to_send: to_send},
            success: function (response) {

            },
        });
    }
});
$(document).on('click', "#submitFormation",function(e){
        e.preventDefault();
        var element = $(this);
        // Envoie de json
        if(typeof url_to_send_to !== "undefined") {
     
            $.ajax({
                type: "POST",
                url: url_to_send_to,
                data: { moduleconfig_forms: moduleconfig_forms},
                success: function (response) {

                },
            });
        }



    });