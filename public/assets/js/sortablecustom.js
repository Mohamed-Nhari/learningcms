$(function() {
    var copy = null
    $(".connectedSortable").sortable({
        connectWith: ".connectedSortable",
        cancel: "[disabled]",
        start: function(e, ui) {
            if (ui.item.closest("ul").attr("id") !== "sortableleft") return
            copy = ui.item.clone().attr({ "disabled": true, "style": ""})
            ui.item.before(copy)
        },
        beforeStop: function(e, ui) {
            if (copy && copy.closest("ul").find(ui.item).length) {
                copy.remove()
                copy = null
            }
        },
        update: function(e, ui) {
            if (!ui.sender) return
            var s = ui.item.attr("data-id");
            var parent = ui.item.closest('ul')
            var matches = parent.find("li[data-target=" + s + "]").length;
            if (matches === 0) {
                ui.sender.sortable("cancel");
                if (copy) {
                    copy.remove()
                }
            }
            copy = null
        }
    }).disableSelection();
});
$(document).ready(function() {
    $(".btn-default8").on("click", function() {
var appenddate=$("#date_certif").val();
$("#daterecuper").empty();
$("#daterecuper").append(appenddate);

var appandname=$("#nomprenom").val();
$("#nameprenomrecup").empty();
$("#nameprenomrecup").append(appandname);

        var appandtext=$("#nametextcertif").val();
        $("#textdescrecuper").empty();
        $("#textdescrecuper").append(appandtext);



    });
});