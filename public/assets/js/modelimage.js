var globalFontstep = {};
$(document).ready(function() {
    $("#file-to-upload-step1 #file").change(function(){
        //download file and save it on the library
        var fd    = new FormData();
        var files = $('#file-to-upload-step1  #file')[0].files[0];
        var typevideo=  $("#typevideoorimage").val();
        if($('#file-to-upload-step1  #file')[0].files[0].type.split('/')[0]==typevideo)
        {
            fd.append('file', files);
            $('#file-to-upload-step1  .value-file').val(files.name);
            uploadFile(fd);
        }
        else{
            alert("Merci de choisir un type correct");
        }


    });
$('.dounload-btn').click(function() {
    $('#dounload').addClass("show");
    $('#gallerie').removeClass('show');
    $('#models').removeClass('show');

})
$('.gallerie-btn').click(function() {
    $('#dounload').removeClass('show');
    $('#gallerie').addClass("show");
    $('#models').removeClass('show');
});
$('.models-btn').click(function() {
    $('#dounload').removeClass('show');
    $('#gallerie').removeClass("show");
    $('#models').addClass("show");
});
    $('#upload-fond-video').click(function(){
        $('#library-modal-step1').attr('data-type','video');
        $('#library-modal-step1 .image').addClass('d-none');
        $('#library-modal-step1 .video').removeClass('d-none');
        $("#typevideoorimage").val("video");
    });
    $('#upload-global-image').click(function(){
        $('#library-modal-step1').attr('data-type','image');
        $('#library-modal-step1 .video').addClass('d-none');
        $('#library-modal-step1.image').removeClass('d-none');
        $("#typevideoorimage").val("image");
    });

    $('#library-modal-step1 .card-img-top').click(function  (){
        $('#library-modal .card-img-top').removeClass('selected')
        $(this).addClass('selected');
    });
    $('#library-modal-step1 #save-file').click(function () {
        var dataType = $('#library-modal-step1').attr("data-type");
        var useconnect =$("#user-id").val();
        var selectedImage = $('#library-modal-step1 .value-file').val();
        var imageFromMedia = $('#library-modal-step1 .card-img-top.selected').attr('data-image-gl');
        $('#library-modal-step1').modal('hide');

        //il we choose image for global font

        if(dataType == 'image') {
            globalFontstep.image = {};
            if(imageFromMedia) {

                globalFontstep.image.filename = imageFromMedia;
                $('#upload-global-image img').attr('src','/images/uploads/'+useconnect+'/documents/'+imageFromMedia);
                $('#upload-global-image').css("background","transparent");
                $('#formation_image_formation').val(globalFontstep.image.filename);
            } else {

                globalFontstep.image.filename = selectedImage;
                console.log(selectedImage);





                    $('#upload-global-image img').attr('src','/images/uploads/'+useconnect+'/documents/'+selectedImage);
                    $('#upload-global-image').css("background","transparent");

                $('#formation_image_formation').val(globalFontstep.image.filename);
            }


        } else if(dataType == "video") {
            //save the video of font on the table globalFont
            var imageFromMedia = $('#library-modal-step1 .card-img-top.selected').attr('data-video-gl');
            globalFontstep.video = {};
            if(imageFromMedia) {
                globalFontstep.video.videoFont = imageFromMedia;
                $('#formation_video_formation').val( globalFontstep.video.videoFont);
                $("#upload-fond-video img").remove();
                $("#advideo").attr('controls', 'true');
                $("#advideo source").attr('src', '/images/uploads/'+useconnect+'/documents/'+selectedImage);
                $("#advideo").css("opacity","1");
                $('#upload-fond-video').css("background","transparent");
            } else {

                globalFontstep.video.videoFont = selectedImage;
                $('#formation_video_formation').val( globalFontstep.video.videoFont);
                $("#upload-fond-video img").remove();
                $("#advideo").attr('controls', 'true');
                $("#advideo source").attr('src', '/images/uploads/'+useconnect+'/documents/'+selectedImage);
                $("#advideo").css("opacity","1");
                $('#upload-fond-video').css("background","transparent");

            }

        }
});
});
function uploadFile(file) {
    $.ajax({
        type: "POST",
        url: '../builder/uploadForBuilder',
        data: file,
        contentType: false,
        processData: false,
        success: function(response){

        },
    });
}