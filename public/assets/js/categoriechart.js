window.onload = function () {
    var counti=2;
    var currentNode = '';
    var clientXY = [0,0];
    OrgChart.templates.ula.pointer = '<g data-pointer="pointer"></g>';
    OrgChart.templates.ula.size = [254, 56];
    OrgChart.templates.ula.node = '<rect x="0" y="0" height="56" width="254" fill="#039BE5" stroke-width="1" stroke="#aeaeae" rx="10" ry="10"></rect>';
    OrgChart.templates.ula.field_0 = '<text width="230" class="field_0"  style="font-size: 18px;" fill="#ffffff" x="127" y="35" text-anchor="middle">{val}</text>';
    OrgChart.templates.ula.edit = '<g data-edit-id="{val}" transform="matrix(1,0,0,1,10,10)"><rect x="0" y="0" width="16" height="16" fill="#039BE5"></rect>' + OrgChart.icon.edit(16,16, '#fff') + '</g>';



    OrgChart.templates.ula.remove = '<g data-remove-id="{val}" transform="matrix(1,0,0,1,30,10)"><rect x="0" y="0" width="16" height="16" fill="#039BE5"></rect>' + OrgChart.icon.remove(16,16, '#fff') + '</g>';
    var chartsed = new OrgChart(document.getElementById("four"), {
        template: "ula",
        enableSearch: false,
        mouseScrool: OrgChart.action.none,
        align: OrgChart.ORIENTATION,
        enableDragDrop: true,
        nodeBinding: {
            field_0: "name",
            edit: 'id',
            remove: 'id'
        },
        tags: {
            "assistant": {
                template: "ula"
            }
        },
        toolbar:{
            zoom:true
        },

    });



    chartsed.on('redraw', function(){
        var editBtns = document.querySelectorAll('[data-edit-id]');
        for(var i = 0; i < editBtns.length; i++){
            editBtns[i].addEventListener('click',  function(){
                chart.editUI.show(this.getAttribute("data-edit-id"));
            });
        }
        var removeBtns = document.querySelectorAll('[data-remove-id]');
        for(var i = 0; i < removeBtns.length; i++){
            removeBtns[i].addEventListener('click',  function(){
                chart.removeNode(this.getAttribute("data-remove-id"));
            });
        }
        var nodeElements = document.querySelectorAll('[node-id]');

        for(var i = 0; i < nodeElements.length; i++){

            var nodeElement = nodeElements[i];
            nodeElement.ondrop = function(ev) {
                ev.preventDefault();

                var id = ev.dataTransfer.getData("id");
                var itemss = document.querySelector('[data-id="' + id + '"]');
                var name = itemss.innerHTML;
                var addclass=itemss.setAttribute("data-id", ""+id + "01"+ "");
                var nodeElement = ev.target;
                while(!nodeElement.hasAttribute('node-id')){
                    nodeElement = nodeElement.parentNode;
                }


                var pid = nodeElement.getAttribute('node-id');
                if (pid!=1){
                    chartsed.addNode({
                        id: id,
                        pid: pid,
                        name: name
                    }, true);
                }




                item.parentNode.removeChild(item);
            }

            nodeElement.ondragover = function(ev) {
                ev.preventDefault();
            }
        }

    });

    chartsed.load([
        {id: 1, name: 'Carnet de notes'},

    ]);

    var items = document.querySelectorAll('.item');
    for(var i = 0; i < items.length; i++){
        var item = items[i];
        item.draggable = true;
        item.ondragstart = function(ev) {
            ev.dataTransfer.setData("id", ev.target.getAttribute('data-id'));
        }
    }
    document.getElementById("btncategorieclik").addEventListener("click", function () {

        var newNodes = [
            { id:counti, pid: 1, name: "Catégorie"+counti },
        ];
        counti++;

        var n = newNodes.splice(0, 1)[0];
        chartsed.addNode(n);

    });

};