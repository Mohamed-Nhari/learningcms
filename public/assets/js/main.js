var k =  1;
var numelment = 1;
var globalFontstep = {};
$(document).ready(function() {
    $(".emoji span").on("click", function(){
        var icone= $(this).data("id-simle");
        $("#icon").val(icone);

    });
    var clicks = 0;
    $("#addrow").click(function () {
        clicks++;
        var $ct = $('#clone-box');
        $('#textBox').val(clicks);
        var i = $("#textBox").val();
        i = parseInt(i) + 1;

        $ct.append('<div class="blocinput"> <label for="indice1">Indice n° ' + i + ' :</label><input type="text" class="form-control" name="" id="demo_' + i + '" value="" /> </div> ')
    });
    var applyFiles = function() {
        if (this.files.length <= 0) {

            $('.guide').show();
        } else {
            $('.choosen').empty();
            $('.guide').hide();

            for (var i = 0; i < this.files.length; ++i) {
                $('.choosen').append($('<li>').html(this.files[i].name));
            }
        }
    }

    $('input[type="file"]').each(function() {
        applyFiles.call(this);
    }).change(function() {
        applyFiles.call(this);
    });
    listCourq = document.getElementById('sortable1');
    listCourq1 = document.getElementById('sortable2');
    if($('#sortable1').length > 0) {
        new Sortable(listCourq, {
            group: 'shared',
            animation: 150
        });
        new Sortable(listCourq1, {
            group: 'shared',
            animation: 150
        });
    }

    if($('#datetpicket').length > 0) {
        console.log("test");
        $('#datetpicket').datetimepicker({
            language: 'fr',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    }

    if($('#dateacces').length > 0) {
        $('#dateacces').datepicker();
    }
    if($('#dataTable').length > 0) {
        $('#dataTable').DataTable({
            paging: false,
            scrollY: 500,
        });
    }

    $('.dataTables_filter label').before('<input id="search-btn" type="checkbox"><label for="search-btn"></label>');
    $('.dataTables_filter label input').attr("id","search-bar");
    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });

    $('.btn-toggle .btn').click(function() {
        $(this).parent().find('.btn').removeClass('active');
        $(this).addClass('active');
    });

    var img = $('.vich-image a').attr('href');

    if (img) {
        $('.img-profile').attr('src', img);
    }
    // Change the type of input to password or text
    $('.show-pass').click(function() {
        var temp = document.getElementById("typepass");
        showPassword(temp, '.show-pass');
    })
    //confirm password inscription
    $('#registration_password_first').click(function() {
        var temp = document.getElementById("registration_password_first");
        showPassword(temp, '.show-pass-1');
    })
    $('#registration_password_second').click(function() {
        var temp = document.getElementById("registration_password_second");
        showPassword(temp, '.show-pass-2');
    })

    $('.show-1 .show-password').click(function() {
        var temp = document.getElementById("profile_password");
        showPassword(temp, '.show-1 .show-password');
    })

    $('.show-2').click(function() {
        var temp = document.getElementById("profile_newPassword_first");
        showPassword(temp, '.show-2');
    })
    $('.show-3').click(function() {
        var temp = document.getElementById("profile_newPassword_second");
        showPassword(temp, '.show-3');
    })
    $('#checkAll').change(function() {
        // this will contain a reference to the checkbox
        if (this.checked) {
            // the checkbox is now checked
            $('.check-element').prop("checked", true);
        } else {
            // the checkbox is now no longer checked
            $('.check-element').prop("checked", false);
        }
    });

    $('.sexRadio').click(function() {
        if (this.previous) {
            this.checked = false;
            $(this).removeClass('check');
        }
        $(this).addClass('check');
        this.previous = this.checked;
    });

    $('.is_checked').click(function() {
        console.log('dark');
    })
    $('.is_unchecked').click(function() {
        console.log('is_unchecked');
    })
    var activeMod = localStorage.getItem('mod');
    console.log(activeMod);
    if (activeMod == "dark") {
        $(this).addClass('checkdark')
        $('body').addClass('dark-mod')
        $('body').find(".toggle_switch .checkbox  svg").attr("class", "is_unchecked");
        $('body').find(".toggle_switch .checkbox svg:first").attr("class", "is_checked");
    } else {
        $('body').find(".toggle_switch .checkbox  svg").attr("class", "is_checked");
        $('body').find(".toggle_switch .checkbox svg:first").attr("class", "is_unchecked");
    }
    $('.switch_3').change(function() {

        if ($(this).hasClass('checkdark')) {
            $(this).removeClass('checkdark')
            $('body').removeClass('dark-mod');
            localStorage.setItem('mod', '');
            $('body').find(".toggle_switch .checkbox  svg").attr("class", "is_checked");
            $('body').find(".toggle_switch .checkbox svg:first").attr("class", "is_unchecked");
        } else {
            $(this).addClass('checkdark')
            $('body').addClass('dark-mod')
            localStorage.setItem('mod', 'dark');
            $('body').find(".toggle_switch .checkbox  svg").attr("class", "is_unchecked");
            $('body').find(".toggle_switch .checkbox svg:first").attr("class", "is_checked");
        }
    })
    //close sidebar
    $("#sidebarToggle, #sidebarToggleTop").on("click", function(o) {
        $("body").toggleClass("sidebar-toggled"), $(".sidebar").toggleClass("toggled"), $(".sidebar").hasClass("toggled") && $(".sidebar .collapse").collapse("hide")
    }), $(window).resize(function() {
        $(window).width() < 768 && $(".sidebar .collapse").collapse("hide")
    }), $("body.fixed-nav .sidebar").on("mousewheel DOMMouseScroll wheel", function(o) {
        if (768 < $(window).width()) {
            var e = o.originalEvent,
                l = e.wheelDelta || -e.detail;
            this.scrollTop += 30 * (l < 0 ? 1 : -1), o.preventDefault()
        }
    }), $(document).on("scroll", function() {
        100 < $(this).scrollTop() ? $(".scroll-to-top").fadeIn() : $(".scroll-to-top").fadeOut()
    }), $(document).on("click", "a.scroll-to-top", function(o) {
        var e = $(this);
        $("html, body").stop().animate({
            scrollTop: $(e.attr("href")).offset().top
        }, 1e3, "easeInOutExpo"), o.preventDefault()
    })
    //disable inscription whenuser dont accepte conditions
    $('#accept-terms').change(function() {
        if (this.checked) {
            $('.btn-user').removeAttr('disabled');
        } else {
            $('.btn-user').attr('disabled', 'disabled');
        }
    });
    $('.user-profile .dropdown-menu .custom-radio, .block-content select').click(function(e) {
        e.stopPropagation();
    });
    $('.monthly-btn').click(function() {
        console.log('click')
        $('#annual').removeClass('show');
        $('#monthly').addClass("show");
    })
    $('.annual-btn').click(function() {
        $('#monthly').removeClass('show');
        $('#annual').addClass("show");
    });

    $('.first-btn').click(function() {
        console.log('click')
        $('#second').removeClass('show');
        $('#first').addClass("show");
    })
    $('.second-btn').click(function() {
        $('#first').removeClass('show');
        $('#second').addClass("show");
    });

    /** delete user confirmation modal **/
    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
    $('#save-folder').click(function() {
        var folder_name = $('#add-folder #folder_name').val();
        if (folder_name) {
            var path = $(this).attr("data-path");
            $.ajax({
                type: "POST",
                url: path,
                data: { folder_name: folder_name },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page and dismiss modal
                        $('#add-folder').modal('hide');
                        location.reload();
                    }
                }
            });
        }
    })
    $('#update-name img').click(function() {
        var id = $(this).parent().attr('data-id');
        var name = $(this).parent().attr('data-name');
        $('#update-folder #folder_name').val(name);
        $('#idfolder').val(id);

    })
    $('#update-folderName').click(function() {
        //get the new name and the folder id
        var folder_name = $('#update-folder #folder_name').val();
        var id = $('#update-folder #idfolder').val();
        if (folder_name) {
            var path = $(this).attr("data-path");
            $.ajax({
                type: "POST",
                url: path,
                data: { folder_name: folder_name, id: id },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page and dismiss modal
                        $('#update-folder').modal('hide');
                        location.reload();
                    }
                }
            });
        }
    })
    $('#delete-folder img').click(function() {
        var id = $(this).parent().attr('data-id');
        var path = $(this).parent().attr("data-path");
        if (id) {
            $.ajax({
                type: "POST",
                url: path,
                data: { id: id },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page
                        location.reload();
                    }
                }
            });
        }
    })
    //duplicate the selectedd folder
    $('#duplicate-folder img').click(function() {
        var folder_name = $(this).parent().attr('data-name')

        if (folder_name) {
            var path = $(this).parent().attr("data-path");
            $.ajax({
                type: "POST",
                url: path,
                data: { folder_name: folder_name },
                success: function(data) {
                    if (data == "success") {
                        //refrech current page
                        location.reload();
                    }
                }
            });
        }
    })
    $('.heart').on('click', function(e) {

        if ($(this).hasClass('full-heart')) {
            $(this).removeClass('full-heart');
        } else {
            $(this).addClass('full-heart');
        }
    })
    $('#delete-all-note').click(function() {
        //get all selected notes
        var notes = [];
        $.each($("input[name='note-check']:checked"), function() {
            notes.push($(this).val());
        });
        var path = $(this).attr("data-path");
        $.ajax({
            type: "POST",
            url: path,
            data: { notes: notes },
            success: function(data) {
                if (data == "success") {
                    location.reload();
                }
            }
        });
    })
    $('#delete-badges').click(function () {
        //get all selected badges
        var badges = [];
        $.each($("input[name='badge-check']:checked"), function () {
            badges.push($(this).attr('data-id'));
        });
        console.log(badges);
        var path = $(this).attr("data-path");
        console.log(path);
        $.ajax({
            type: "POST",
            url: '/formation/deleteBadges',
            data: {badges: badges},
            success: function (data) {
                if (data == "success") {
                    location.reload();
                }
            }
        });
    });
    $('#tab-formations').click(function() {
        $('.tab-forma').removeClass('hidden');
        $('.list-forma').addClass('hidden');
        var activeImage = $(this).find('img').attr('data-activeimage');
        var nonActive = $('#list-formations img').attr('data-nonactiveimage');
        $(this).find('img').attr('src', activeImage);
        $('#list-formations img').attr('src', nonActive);

    });

    $('#list-formations').click(function() {
        $('.tab-forma').addClass('hidden');
        $('.list-forma').removeClass('hidden');
        var activeImage = $(this).find('img').attr('data-activeimage');
        var nonActive = $('#tab-formations img').attr('data-nonactiveimage');
        $(this).find('img').attr('src', activeImage);
        $('#tab-formations img').attr('src', nonActive);
    });


    //remove folder for others user for the current user on the page note we can find a solution by dev but this is a similar solution
    $('#form_studentFolder > option').each(function() {
        if (!$(this).text()) {
            $(this).remove();
        }
    });

    var showPassword = function($temp, $class) {
        if ($temp.type === "password") {
            $temp.type = "text";
            $($class).find('.fa-eye').addClass('fa-eye-slash').removeClass('fa-eye');
        } else {
            $temp.type = "password";
            $($class).find('.fa-eye-slash').addClass('fa-eye').removeClass('fa-eye-slash');
        }
    }

    $('#click-lamp').click(function() {
        $('#lamp-modal .default-content, #lamp-modal .default-content, #lamp-modal .default-title').addClass('hidden');
        $('#lamp-modal .success-title, #lamp-modal .new-content').removeClass('hidden');
    })
    $('#add-config').click(function(){
        var nbModule  = $('.nb-module').val();
        $('#list-module').html("");
        for(var i = 1; i<= nbModule; i++) {
           // var module_popup = $("#moduleconfig").clone();

           // $("#moduleconfig").after($(module_popup).attr('id', "moduleconfigitemdat-"+i));
            $('#list-module').append('<div class="blocgloab"><div class="blocimage">' +
                ' <button class="nobutton cloner" ><img src="/assets/images/icons/formation/15.png"/></button>' +
                '<button class="nobutton loop"><img src="/assets/images/icons/formation/16.png"/></button>' +
                '<button class="nobutton delete" ><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#moduleconfigitemdat" class="nobutton"><img src="/assets/images/icons/formation/19.png"/></button><button class="nobutton edit"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hovermodule'+i+'" data-id="itemdat-'+i+'" class="item ">Module '+i+' <input type="number" value="0"/> </a>      </div></div>')
        }



        var nbCour    = $('.nb-cour').val();
        $('#list-cour').html("");
        for(var i = 1; i<= nbCour; i++) {

            $('#list-cour').append('<div class="blocgloab"><div class="blocimage">' +
                '<button class="nobutton cloner1" ><img src="/assets/images/icons/formation/15.png"/></button>' +
                '<button class="nobutton loop2"><img src="/assets/images/icons/formation/16.png"/></button>' +
                '<button class="nobutton delete1"><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#courspopup"class="nobutton"> <img src="/assets/images/icons/formation/19.png"/><button class="nobutton edit1"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hovermodule'+i+'" data-id="itemcour-'+i+'" class=" item">Cours '+i+' <input type="number" value="0"/> </a>      </div></div>')
        }


            var nbChapter = $('.nb-chapter').val();
            $('#list-chap').html("")


    for(var i = 1; i<= nbChapter; i++) {
        $('#list-chap').append('<div class="blocgloab"><div class="blocimage">' +
            '<button class="nobutton cloner2" ><img src="/assets/images/icons/formation/15.png"/></button>' +
            '<img src="/assets/images/icons/formation/16.png"/></button>' +
            '<button class="nobutton delete2"><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#chaiptre" class="nobutton"><img src="/assets/images/icons/formation/19.png"/><button class="nobutton edit2"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hovermodule'+i+'" data-id="itemshap-'+i+'" class=" item">Chap '+i+' <input type="number" value="0"/> </a>      </div></div>')
    }
    var nbQuiz    = $('.nb-quiz').val();
    $('#list-quiz').html("");
    for(var i = 1; i<= nbQuiz; i++) {
        $('#list-quiz').append('<div class="blocgloab"><div class="blocimage">' +
            '<button class="nobutton cloner3" ><img src="/assets/images/icons/formation/15.png"/></button>' +
            '<img src="/assets/images/icons/formation/16.png"/></button>' +
            '<button class="nobutton delete3"><img src="/assets/images/icons/formation/17.png"/></button><button type="button" data-toggle="modal" data-target="#quizs" class="nobutton"><img src="/assets/images/icons/formation/19.png"/><button class="nobutton edit3"><img src="/assets/images/icons/formation/14.png"/></button></div><div id="hovermodule'+i+'" data-id="itemsqiz-'+i+'" class=" item">Quiz '+i+' <input type="number" value="0"/> </a>      </div></div>')


    }
})


//Update user status

$('.activate-user').change(function() {
    var id     = $(this).attr('data-id');
    if (this.checked) {
        // activate user
        var active = 1;
    } else {
        // desactivate user
        var active = 0;
    }

    $.ajax({
        type: "POST",
        data: { id: id, active: active },
        url: '/admin/changeStatus',
        success: function(data) {
        }
    });
});
$('.modal-delete-user').click(function(e) {
    e.preventDefault();
    $('.td-actions .modal-delete-user').removeClass('active-user');
    $(this).addClass('active-user');
})
$('.delete-user').click(function(e) {
    e.preventDefault();
    var id = $('.td-actions .active-user').attr('data-id');
    $.ajax({
        type: "POST",
        data: { id: id},
        url: '/admin/deleteUser',
        success: function(data) {
            location.reload();
        }
    });
})

$('.activate_all_user').change(function() {
    var users = [];
    $.each($("input[name='user_check']:checked"), function() {
        users.push($(this).attr('data-id'));
    });
    if (this.checked) {
        // activate users
        $.ajax({
            type: "POST",
            url: "/admin/activateAlluser",
            data: { users: users },
            success: function(data) {
                if (data == "success") {
                    //location.reload();
                }
            }
        });
        $.each($("input[name='user_check']:checked"), function() {
            var id = $(this).attr('data-id');
            $( ".activate-user" ).each(function( index) {
                if($(this).data('id') == id){
                    $(this).attr('checked','checked');
                }
            });
        });
    } else {
        // desactivate users
        $.ajax({
            type: "POST",
            url: "/admin/desactivateAlluser",
            data: { users: users },
            success: function(data) {
                if (data == "success") {
                    //location.reload();
                }
            }
        });
        $.each($("input[name='user_check']:checked"), function() {
            var id = $(this).attr('data-id');
            $( ".activate-user " ).each(function( index ) {
                if($(this).data('id') == id){
                    $(this).removeAttr('checked');
                }
            });
        });
    }
});

$('#delete-selected-user').click(function() {
    //get all selected notes
    var users = [];
    $.each($("input[name='user_check']:checked"), function() {
        users.push($(this).attr('data-id'));
    });
    var path = $(this).attr("data-path");
    $.ajax({
        type: "POST",
        url: "/admin/deleteAllUser",
        data: { users: users },
        success: function(data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
});
if($('.carousel').length > 0) {
    $('.carousel').carousel({
        wrap: false
    })
}

$('.delete-image').click(function(e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var path = $(this).attr("data-path");
    console.log( id);
    if (id) {
        $.ajax({
            type: "POST",
            url: path,
            data: { id: id },
            success: function(data) {
                if (data == "success") {
                    $.ajax({
                        type: "POST",
                        url: '/admin/getMedia',
                        success: function(data) {
                            $('#file-view').html(data);
                        }
                    });
                }
            }
        });
    }
})


$('.copie-link').click(function(e) {
    e.preventDefault();
});

$('.update-fileName').click(function(e) {
    e.preventDefault();
    var mediaId = $(this).attr('data-id');
    $("#wrapper").removeClass("toggled");

    if ($('#file-view #media-' + mediaId).hasClass('active')) {
        $('#file-view #media-' + mediaId).removeClass('active');
        $("#wrapper").addClass("toggled");
    } else {
        $('#file-view .active').removeClass('active');
        $('#file-view #media-' + mediaId).addClass('active');
    }

    if($("#wrapper").hasClass('toggled')) {
        $.ajax({
            type: "POST",
            url: '/admin/getFolder/8',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
    } else {
        $.ajax({
            type: "POST",
            url: '/admin/getMedia/'+mediaId,
            data: { folderId: '', },
            success: function(data) {
                $('#sidebar-wrapper').html(data);
            }
        });
        //update the folders displayed data
        $.ajax({
            type: "POST",
            url: '/admin/getFolder/6',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
    }
});


$(".menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});


$('#add-folder-doc').click(function() {
    var folder_name = $('#add-folder #folder_name').val();
    if (folder_name) {
        var path = $(this).attr("data-path");
        $.ajax({
            type: "POST",
            url: path,
            data: { folder_name: folder_name },
            success: function(data) {
                if (data == "success") {
                    //refrech current page and dismiss modal
                    $('#add-folder').modal('hide');
                    location.reload();
                }
            }
        });
    }
})

$('#update-folderDoc').click(function() {
    //get the new name and the folder id
    var folder_name = $('#update-folder #folder_name').val();
    var id          = $('#update-folder #idfolder').val();
    if (folder_name) {
        var path = $(this).attr("data-path");
        $.ajax({
            type: "POST",
            url: path,
            data: { folder_name: folder_name, id: id },
            success: function(data) {
                if (data == "success") {
                    //refrech current page and dismiss modal
                    $('#update-folder').modal('hide');
                    location.reload();
                }
            }
        });
    }
});
$('#delete-folder-doc img').click(function() {
    var id = $(this).parent().attr('data-id');
    var path = $(this).parent().attr("data-path");
    if (id) {
        $.ajax({
            type: "POST",
            url: path,
            data: { id: id },
            success: function(data) {
                if (data == "success") {
                    //refrech current page
                    location.reload();
                }
            }
        });
    }
});
$('#carousel-folder .full-card').hover(
    function(){ $(this).addClass('hover') },
    function(){ $(this).removeClass('hover') }
);
$('#carousel-folder .full-card').click(function(e) {
    e.preventDefault();
    var folderId = $(this).attr('data-id');
    //add class active to the current folder
    if($(this).hasClass('active')) {
        $(this).removeClass('active');
        folderId = '';
    } else {
        $('#carousel-folder .full-card').removeClass('active');
        $(this).addClass('active');
    }
    $.ajax({
        type: "POST",
        url: '/admin/getMedia',
        data: { folderId: folderId },
        success: function(data) {
            $('#file-view').html(data);
        }
    });
})

$('#file-view .main').hover(
    function(){ $(this).addClass('hover') },
    function(){ $(this).removeClass('hover') }
);

$('#delete-all-file').click(function() {
    //get all selected notes
    var files = [];
    $.each($("input[name='file-check']:checked"), function() {
        files.push($(this).val());
    });
    var path = $(this).attr("data-path");
    $.ajax({
        type: "POST",
        url: path,
        data: { files: files },
        success: function(data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
})

$('.a-declarative').hover(
    function(){ $(this).addClass('hover') },
    function(){ $(this).removeClass('hover') }
);


if($(".select2").length > 0) {
    $(".select2").select2({
        minimumResultsForSearch: -1
    });
}
if($('select.form-select').length > 0) {
    $('select.form-select').select2({
        minimumResultsForSearch: -1
    });
}
if($(".select-color").length > 0) {
    $(".select-color").select2({
        minimumResultsForSearch: -1
    });
}

if($('.select_2').length > 0) {
    $("#profile_birthday_day, #profile_birthday_month, #profile_birthday_year").select2({
        minimumResultsForSearch: -1
    })
}
$('#formationconfig_enable_hours').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
        $('#sub-delay').removeClass('hidden');
    } else {
        // the checkbox is now no longer checked
        $('#sub-delay').addClass('hidden')
    }
});

$('#edit-title span').click(function() {
    $('#modal-title').prop("disabled", false);
    $('#modal-title').removeClass('disabled')
})
$('#edit-text span').click(function() {
    $('#modal-text').prop("disabled", false);
    $('#modal-text').removeClass('disabled')
})
$('#edit-btn span').click(function() {
    $('#modal-btn').prop("disabled", false);
    $('#modal-btn').removeClass('disabled')
})

$('.logout').click(function (e) {
    var result = $('#timer').text();
    e.preventDefault();
    var counter = result;
    console.log(counter);
    $.ajax({
        type: "POST",
        url: "/admin/updateCounter",
        data: {counter: counter, expired: false},
        success: function (data) {
            //location.href = 'http://learningcms.test:8000/login';
            console.log(data);
        }
    });
});

/** module user **/
$('#filter-user').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
    } else {
        // the checkbox is now no longer checked
    }
});
/** end module user **/

/** set user active time **/
// Set timeout variables.
StartTimers();
document.onmousemove = function(e) {
    ResetTimers();
}

$('.show-grid').click(function() {
    $('.type-grid').removeClass('update-grid');
    if($('.type-grid').hasClass('hidden')) {
        $('.type-grid').removeClass('hidden');
    } else {
        $('.type-grid').addClass('hidden');
    }
})

$('.type-grid .et-fb-settings-options-wrap ul li').click(function() {
    var selectedRow = $('.type-grid').attr('data-row_id');

    $('.show-grid.hidden').removeClass('hidden');
    $('.first-img').addClass('hidden');

    $('.default-content').addClass('newElment');
    var cols  = $(this).attr('data-layout');

    var list  = cols.split(',');
    var customHtml ='<div class ="row connectedsortable ml-1 mr-1" id="row-'+k+'"><div class="action-col"><a href="#"><img src="/assets/images/builder/zoom.png"></a>'+
        '<a href="#" onclick="showConfigSidebar('+k+')" ><img src="/assets/images/builder/edit-col.png"></a><a href="#" onclick="duplicateRow('+k+')"><img src="/assets/images/builder/duplicate.png"></a>'+
        '<a href="#" onclick="updateRow('+k+')"><img src="/assets/images/builder/update-row.png"></a><a href="#"  onclick="updateVisibility('+k+')" ><img src="/assets/images/builder/detail.png"></a><a href="#"><img src="/assets/images/builder/download-sm.png"></a>'+
        '<a href="#" class="delete-row" onclick="deleteRow('+k+')"><img src="/assets/images/builder/trash-sm.png"></a></div>';

    var elemntWidh = 0;
    list.forEach(function(element) {
        var widthCol = element.split('_');
        var first = widthCol[0];
        var second = widthCol[1];
        elemntWidh = 100 /second * first;
        // construre the html elment
        customHtml += '<div class="list-group-item col-item m-0" style="width: '+elemntWidh+'%"></div>';
    });
    customHtml +='</div>';

    if(selectedRow) {
        var listCols = $('#row-'+selectedRow +' .col-item');
        var sumWidh  = 0;
        for (var i = 0; i < listCols.length; i++) {
            listCols[i].setAttribute('style','width:'+elemntWidh+'%');
            sumWidh+=elemntWidh;
            delete list[ i ];
        }
        if(list.length > 0 && listCols.length < list.length) {

            list.forEach(function(element) {
                var widthCol = element.split('_');
                var first = widthCol[0];
                var second = widthCol[1];
                elemntWidh = 100 /second * first;
                // construre the html elment
                var newCol = '<div class="list-group-item col-item m-0" style="width: '+elemntWidh+'%"></div>';
                $('#row-'+selectedRow).append(newCol);
            });
            $('.type-grid').attr('data-row_id','');
        }

    } else {
        $('.bulider-content').append(customHtml);
    }

    $('.type-grid').addClass('hidden');
    var tds = $('.bulider-content .col-item');
    tds.each(function(index, element) {
        new Sortable(element, {
            group: {
                name: 'shared',

            },
            animation: 150,
        });
    });
    k++;
});
var uploadImage = document.getElementById('upload-image');
if($('#upload-image').length >0) {
    new Sortable(uploadImage, {
        group: {
            name: 'shared',
            pull: 'clone'
        },
        animation: 150,
        onEnd: function (subevt) {
            $('.custom-row').removeClass('new-row');
            customHtml = '<div class="updateElment" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
                '<a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a>'+
                '<a href="#" onclick="showConfigSidebar('+numelment+')""><img src="/assets/images/builder/bagette-magic.png"></a>'+
                '<a href="#" onclick="duplicateCol('+numelment+')"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a>'+
                '<a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
                '<a href="#" class="delete-row" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
            ;
            $('.bulider-content').find('.d-none').removeClass('d-none').addClass('new-row');
            $('.bulider-content .custom-file-upload').remove();
            $('.new-row .col-12').append(customHtml);
            numelment++;
        }
    });
}
var getComposition = document.getElementById('get-composition');
if($('#get-composition').length >0) {
    new Sortable(getComposition, {
        group: {
            name: 'shared',
        },
        animation: 150
    });
}
var listComposition = document.getElementById('list-composition');
if($('#list-composition').length > 0) {
    var j = 1;
    new Sortable(listComposition, {
        group: {
            name: 'shared',
            pull: 'clone'
        },
        animation: 150,
        onStart: function( event, ui ) {
            console.log('start');
            //$(ui.item).append("<div>abc<br><br>abc</div>");
        },
        onEnd: function (subevt) {
            $('.custom-row').removeClass('new-row');
            $('.bulider-content').find('.d-none').removeClass('d-none').addClass('new-row');;
            $('.bulider-content .comp>img').remove();
            $.each($(".new-row .col"), function(index, element) {
                customHtml = '<div class="updateElment" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
                    '<a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a><a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/bagette-magic.png"></a>'+
                    '<a href="#"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a><a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
                    '<a href="#" id="col-'+index+'" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
                ;$(this).append(customHtml);
                numelment++;
            });
            j++;
        }
    });
}

$('.no-collapsable').on('click', function (e) {
    e.stopPropagation();
});

$('#list-media').click(function(){
    $('.all-modules').addClass('hidden');
    $('.medias, .get-media').removeClass('hidden');

});
$('#choose-image').click(function() {
    $('.medias').addClass('hidden');
    $('#upload-image, .get-img').removeClass('hidden');
    $('#composotion').collapse('show');
});
$('.get-bloc').click(function(e){
    e.stopPropagation();
    e.preventDefault();
    $('.get-media,.get-img').addClass('hidden');
    //$('.get-img').remove();

    $('.all-modules').removeClass('hidden');
    $('.medias, #upload-image').addClass('hidden');

    $('#composotion').collapse('hide');
});
$('.get-media').click(function(e){
    e.stopPropagation();
    e.preventDefault();

    $('.all-modules,#upload-image').addClass('hidden');
    $('.medias').removeClass('hidden');
    $('.get-img').addClass('hidden');
    $('#composotion').collapse('hide');
});
$('#edit-font').click(function() {
    if($('#config-sidebar').hasClass('d-none')) {
        $('.custom-side').addClass('d-none');
        $('#config-sidebar').removeClass('d-none');
        $('#main-content').removeClass('col-10');
        $('#main-content').addClass('col-8');
        $('.pcr-app').addClass('visible');
        const pickr3 = new Pickr({
            el: '#color-picker-3',
            useAsButton: true,
            default: "303030",
            showAlways: true,
            components: {
                preview: true,
                opacity: true,
                hue: true,

                interaction: {
                    input: true,
                }
            },

            onChange(hsva, instance) {
                $('.appercu-color').css('background-color', hsva.toRGBA().toString());
            }
        });
        $('.pcr-picker').css('top', '63.753px');
    } else {

        $('#config-sidebar').addClass('d-none');
        $('#main-content').removeClass('col-8');
        $('#main-content').addClass('col-10');
        $('.pcr-app').removeClass('visible');
    }
})


$('.list-sttaic-color span').click(function() {
    var color = $(this).attr('data-color');
    $('.appercu-color').css('background-color', color);
    $('.appercu-color').attr('data-color',color);
    //get the seleced row
    /*var idElment = $('#magic-sidebar #selected-elment').attr('data-selected-elment');
    $('#row-'+idElment+' .col-item').css('background-color', color);*/
})

/** end document ready **/
});
function deleteCol(key) {
    $('#elment-'+key).parent().html('');
}
function duplicateCol(key) {
    var toDuplicate = $('#elment-'+key).parent().html();
    $('#elment-'+key).parent().after('<div class="col-12 colned">'+toDuplicate+'</div>');
    $('.colned .updateElment').remove();

    customHtml = '<div class="updateElment" id="elment-'+numelment+'"><img src="/assets/images/builder/change-pos.png"></a>'+
        '<a href="#" onclick="showConfigSidebar('+numelment+')"><img src="/assets/images/builder/pencil-cl.png"></a>'+
        '<a href="#" onclick="showConfigSidebar('+numelment+')""><img src="/assets/images/builder/bagette-magic.png"></a>'+
        '<a href="#" onclick="duplicateCol('+numelment+')"><img src="/assets/images/builder/duplicate-cl.png"></a><a href="#" onclick="updateVisibility('+numelment+')"><img src="/assets/images/builder/show-cl.png"></a>'+
        '<a href="#"><img src="/assets/images/builder/download-cl.png"></a>'+
        '<a href="#" class="delete-row" class="delete-col" onclick="deleteCol('+numelment+')"><img src="/assets/images/builder/drash-cl.png"></a></div>'
    ;
    $('.colned').append(customHtml);
    $('.cloned').removeClass('cloned');
    numelment++
}
window.onbeforeunload = function(event)
{
    //save counter on local storage
    //localStorage.setItem('counter', userTime);
    //var userTimer = localStorage.getItem('counter');

};

/** set varaible to use to update user counter **/
//this varaible it can be updatted by admin
var timoutNow = 30000; // Timeout in 15 mins.
var timeoutTimer;

var timer;

var sec  = 0;
var min  = 0;
var hour = 0;
var userTime = 0;
timer = setInterval(timeHandler, 1000);

/** functions**/

function timeHandler() {
    sec++;
    if (sec == 60) {
        sec = 0;
        min++;
    }

    if (min == 60) {
        min = 0;
        hour++;
    }

    displayTime();
}

function displayTime() {

    let time = document.getElementById('timer');
    let currentMin = min;
    let currentHour = hour;

    if (sec < 10) sec = `0${sec}`;
    if (min < 10) currentMin = `0${min}`;
    if (hour < 10) currentHour = `0${hour}`;

    //time.innerHTML = `${currentHour}:${currentMin}:${sec}`
}

// Start timers.
function StartTimers() {
    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
}
// Reset timers.
function ResetTimers() {
    clearTimeout(timeoutTimer);
    StartTimers();
}
// save the timer on local storage to use it on all the framework

// Save time counter.
function IdleTimeout() {
    var result = $('#timer').text();
    var counter = result;
    $.ajax({
        type: "POST",
        url: "/admin/updateCounter",
        data: { counter: counter },
        success: function(data) {
            console.log(data);
        }
    });
}
function deleteRow(index) {
    console.log(index);
    $('#row-'+index).remove();
    var hasContent = $('.bulider-content').html();

    if($.trim($(".bulider-content").html())=='') {
        $('.default-content').removeClass('newElment');
        $('.show-grid').addClass('hidden');
        $('.first-img').removeClass('hidden');
    }
}
function duplicateRow(index) {
    var itemToDuplicate =  $('#row-'+index).html();
    $('.bulider-content #row-'+index).after('<div class ="row connectedsortable ml-1 mr-1" id="row-'+(parseInt(index)+k-1)+'">'+itemToDuplicate+'</div>');
}
function updateRow(index) {
    if( $('.type-grid').hasClass('hidden')) {
        $('.type-grid').removeClass('hidden');
        $('.type-grid').addClass('update-grid');
    } else {
        $('.type-grid').addClass('hidden');
        $('.type-grid').removeClass('update-grid');
    }
    $('.type-grid').attr('data-row_id',index);
}
function updateVisibility(index) {

    //if($('#magic-sidebar').hasClass('d-none')) {
    $('.custom-side').addClass('d-none');
    $('#magic-sidebar').removeClass('d-none');
    $('#main-content').removeClass('col-10');
    $('#main-content').addClass('col-8');
    $('.pcr-app').removeClass('visible');
    $('.effect-btn').addClass('active');
    $('.visibility-btn').removeClass('active');
    $('#effect').addClass('show');
    $('#visibility').removeClass('show');

    $('.effect-btn').removeClass('active');
    $('.visibility-btn').addClass('active');
    $('#effect').removeClass('show');
    $('#visibility').addClass('show');
    /*} else {
        $('.custom-side').addClass('d-none');
        $('#main-content').removeClass('col-8');
        $('#main-content').addClass('col-10');
    }*/

}

function showConfigSidebar(rowId) {
    console.log(rowId);
    $('#selected-elment').attr('data-selected-elment',rowId);

    //if($('#magic-sidebar').hasClass('d-none')) {
    $('.custom-side').addClass('d-none');
    $('#magic-sidebar').removeClass('d-none');
    $('#main-content').removeClass('col-10');
    $('#main-content').addClass('col-8');
    $('.pcr-app').removeClass('visible');
    $('.effect-btn').addClass('active');
    $('.visibility-btn').removeClass('active');
    $('#effect').addClass('show');
    $('#visibility').removeClass('show');

    /*} else {
        $('.custom-side').addClass('d-none');
        $('#main-content').removeClass('col-8');
        $('#main-content').addClass('col-10');
    }*/

}
/** this function allow user to create a multipel checkbox listing **/
function checkboxDropdown(el='', text="Ajouter au(x) dossier(s)") {
    var $el = $(el)

    function updateStatus(label, result, folders) {
        if(!result.length) {
            //label.html(text);
        }
    };

    $el.each(function(i, element) {
        var $list = $(this).find('.dropdown-list'),
            $label = $(this).find('.dropdown-label'),
            $inputs = $(this).find('.check'),
            defaultChecked = $(this).find('input[type=checkbox]:checked'),
            result = [];
        folders = [];
        htmlResult = [];
        htmlResult2 = [];

        updateStatus($label, result);
        if(defaultChecked.length) {
            defaultChecked.each(function () {
                if($(this).parent().parent().hasClass('catalogue-list')) {
                    htmlResult.push('<span>'+$(this).next().text()+'</span>');

                } else {
                    htmlResult2.push('<span>'+$(this).next().text()+'</span>');
                }
                result.push($(this).next().text());
                htmlResult.push('<span>'+$(this).next().text()+'</span>');



                if(el != ".dropdown-select") {
                    $label.html(result.join(", "));
                }else {
                    if($(this).parent().parent().hasClass('catalogue-list')){
                        console.log(htmlResult);
                        $('.catalogue .list-selection').html(htmlResult.join(" "));


                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));

                    }

                }

                folders.push($(this).next().attr('data-id'));
            });
        }

        $label.on('click', ()=> {
            $(this).toggleClass('open');
        });

        $inputs.on('change', function() {
            var checked = $(this).is(':checked');
            var checkedText = $(this).next().text();
            var checkedID= $(this).next().attr('data-id');
            if(checked) {
                result.push(checkedText);
                if($(this).parent().parent().hasClass('catalogue-list')) {
                    htmlResult.push('<span>'+checkedText+'</span>');
                }else {
                    htmlResult2.push('<span>'+checkedText+'</span>');
                }


                if(el != ".dropdown-select") {
                    $label.html(result.join(", "));
                }else {
                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        $('.catalogue .list-selection').html(htmlResult.join(" "));
                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));
                    }
                }

                folders.push(checkedID);
            }else{
                let index = result.indexOf(checkedText);
                if (index >= 0) {
                    result.splice(index, 1);
                    folders.splice(index, 1);

                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        htmlResult.splice(index, 1);
                    }else {
                        htmlResult2.splice(index, 1);
                    }
                }
                if(el != ".dropdown-select") {
                    $label.html(result.join(", "));
                } else {
                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        $('.catalogue .list-selection').html(htmlResult.join(" "));
                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));
                    }

                }
            }
            updateStatus($label, result, folders);
            $('#dropdown-folder, #choose-folder').attr('data-ids', folders);
        });
    });
};

function filePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.vich-image  img, .img-profile').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#profile_imageFile_file").change(function() {
    filePreview(this);
});

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

$(window).resize(function(e) {
    /*if ($(window).width() <= 768) {
       $.ajax({
            type: "POST",
            url: '/admin/getFolder/4',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
       $('.navbar-nav').addClass('toggled');
    } else if ($(window).width() <= 920) {
       $.ajax({
            type: "POST",
            url: '/admin/getFolder/6',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
    } else {
        $.ajax({
            type: "POST",
            url: '/admin/getFolder/8',
            success: function(data) {
                $('#carousel-folder').html(data);
            }
        });
    } */
});
if ($(window).width() <= 920) {
    $('.navbar-nav').addClass('toggled');
    $.ajax({
        type: "POST",
        url: '/admin/getFolder/4',
        success: function(data) {
            $('#carousel-folder').html(data);
        }
    });
}
if ($(window).width() <= 1250) {
    $.ajax({
        type: "POST",
        url: '/admin/getFolder/6',
        success: function(data) {
            $('#carousel-folder').html(data);
        }
    });
}


checkboxDropdown('.custom-dropdown');

/** end multicheckbox **/


checkboxDropdown('.dropdown-select', 'Sélection');
checkboxDropdowns('#formdrown', 'Sélection');


/** end functions **/
if($(".select2").length > 0) {
    $(".select2").select2({
        minimumResultsForSearch: -1
    });
}

if($('.select_2').length > 0) {
    $("#profile_birthday_day, #profile_birthday_month, #profile_birthday_year").select2({
        minimumResultsForSearch: -1
    })
}
$('#delay').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
        $('#sub-delay').removeClass('hidden');
    } else {
        // the checkbox is now no longer checked
        $('#sub-delay').addClass('hidden')
    }
});

$('#edit-title span').click(function() {
    $('#modal-title').prop("disabled", false);
    $('#modal-title').removeClass('disabled')
})
$('#edit-text span').click(function() {
    $('#modal-text').prop("disabled", false);
    $('#modal-text').removeClass('disabled')
})
$('#edit-btn span').click(function() {
    $('#modal-btn').prop("disabled", false);
    $('#modal-btn').removeClass('disabled')
})
//if($('#put-file').length > 0) {

//}

$('.first-btn').click(function() {
    $('#second').removeClass('show');
    $('#first').addClass("show");
})
$('.second-btn').click(function() {
    $('#first').removeClass('show');
    $('#second').addClass("show");
});

/** module user **/
$('#filter-user').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
    } else {
        // the checkbox is now no longer checked
    }
});
/** end module user **/
function search(key, type) {
    var settings = {
        /**
         *Mandatory
         *Text field ID where you want to implement autocomplete.
         */
        inputId: type,
        /**
         *optional
         *No. of length when keyup event fire.
         */
        inputLength: 1,

        /**
         *optional
         *Json Field name which you want to put in text field when selected.
         */
        inputValue: 'name',

        /**
         *optional
         *Fields name from json data which you want to show in table rest fields will hide.
         */
        fields: 'name',

        /**
         *optional
         *If you have json data in your page no need to pass ajax url.
         *just pass associative json in data
         */
        data:  [{'name': "red"},{'name': "green"},{'name': "blue"},{'name': "gray"}]
    }

    /**
     *1st Param : settings
     *2nd Param : onSelectCallback, it is fired when select on autosuggestion with selected data
     */
    AjaxLiveSearch.init(settings, function (data) {
        $('.my-result-'+type).append('<span class="col-6">'+data.name+'<a href=""> <img src="/assets/images/icons/close.png"></a></span>');

        $('.search').val('');
    });
}

$('.effect-btn').click(function() {
    $('#visibility').removeClass('show');
    $('#effect').addClass("show");
})
$('.visibility-btn').click(function() {
    $('#effect').removeClass('show');
    $('#visibility').addClass("show");
});
$('#typestep').on("change", function(e) {
    if($("#typestep").length > 0){
        var s = $("#typestep").select2('val');
        console.log(s);
        if (s == "Devoir") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Devoir");
            $('#dev-3').removeClass('hidden');
            $('#dev-5').removeClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Valider") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Valider");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').removeClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Quiz") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Quiz");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').removeClass('hidden');
            $('#quiz-5').removeClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
        }
        if (s == "Score") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Score");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').removeClass('hidden');
            $('#score-8').removeClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Temps") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Temps");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').removeClass('hidden');
            $('#temps--4').removeClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Forum") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Forum");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').removeClass('hidden')
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Note") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Note");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').removeClass('hidden');
            $('#note--4').removeClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').addClass('hidden');
            $('#recomp--4').addClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
        if (s == "Récompenses") {
            $('#modal-title').empty();
            $('#modal-title').append("Condition - Récompenses");
            $('#dev-3').addClass('hidden');
            $('#dev-5').addClass('hidden');
            $('#valid-8').addClass("hidden");
            $('#quiz-3').addClass('hidden');
            $('#quiz-5').addClass('hidden');
            $('#score-6').addClass('hidden');
            $('#score-8').addClass('hidden');
            $('#temps-4').addClass('hidden');
            $('#temps--4').addClass('hidden');
            $('#note-4').addClass('hidden');
            $('#note--4').addClass('hidden');
            $('#forum-4').addClass('hidden');
            $('#recomp-4').removeClass('hidden');
            $('#recomp--4').removeClass('hidden');
            $('#devoir-val').addClass('hidden');
            $('#dev-color').addClass('hidden');
            $('#devoir-otr').addClass('hidden');
            $('#quiz-dev').addClass('hidden');
        }
    }
});
$('#badge-devoir').on("change", function(e) {
    var d = $("#badge-devoir").select2('val');
    console.log(d);
    if (d == "Appréciation") {
        $('#dev-5').removeClass('hidden');
        $('#devoir-val').addClass('hidden');
        $('#dev-color').addClass('hidden');
        $('#devoir-otr').addClass('hidden');
    } else if (d == "Chiffré") {
        $('#devoir-val').removeClass('hidden');
        $('#dev-5').addClass('hidden');
        $('#dev-color').addClass('hidden');
        $('#devoir-otr').addClass('hidden');
    } else if (d == "Couleur") {
        $('#dev-color').removeClass('hidden');
        $('#dev-5').addClass('hidden');
        $('#devoir-val').addClass('hidden');
        $('#devoir-otr').addClass('hidden');
    } else if(d=="Autre"){
        $('#devoir-otr').removeClass('hidden');
        $('#dev-5').addClass('hidden');
        $('#devoir-val').addClass('hidden');
        $('#dev-color').addClass('hidden');
    }
});
$('#badge-quiz').on("change", function(e) {
    var d= $("#badge-quiz").select2('val');
    if (d=="Certificats"){
        $('#quiz-dev').removeClass('hidden')
        $('#quiz-5').addClass('hidden');
    }else {
        $('#quiz-dev').addClass('hidden');
        $('#quiz-5').removeClass('hidden');
    }
});
$(".open-button").on("click", function() {
    $(this).closest('.collapse-group').find('.collapse').collapse('show');
});

$(".close-button").on("click", function() {
    $(this).closest('.collapse-group').find('.collapse').collapse('hide');
});
if($('#color-picker-2').length > 0) {
    const pickr2 = new Pickr({
        el: '#color-picker-2',
        useAsButton: true,
        default: "303030",
        showAlways: true,
        components: {
            preview: true,
            opacity: true,
            hue: true,

            interaction: {
                input: true,
            }
        },

        onChange(hsva, instance) {
            $('.appercu-color').css('background-color', hsva.toRGBA().toString());
        }
    });
    $('.pcr-picker').css('top', '63.753px');
    $('.pcr-app').removeClass('visible');
}
$('#show-font').click(function() {
    if($('.pcr-app').hasClass('visible')) {
        $('.pcr-app').removeClass('visible');
    } else {
        $('.pcr-app').addClass('visible');
    }
})
if($("#ex8").length > 0) {
    $("#ex8").slider();
    $("#ex8").on("slide", function(slideEvt) {
        $("#ex6SliderVal").text(slideEvt.value);
    });
}
if($('#sliderPosition').length > 0) {
    $("#sliderPosition").slider();
    $("#sliderPosition").on("slide", function(slideEvt) {
        $("#sliderPositionVal").text(slideEvt.value);
    });
}
if($('#sliderPositionfin').length > 0) {
    $("#sliderPositionfin").slider();
    $("#sliderPositionfin").on("slide", function(slideEvt) {
        $("#sliderPositionfinVal").text(slideEvt.value);
    });
}
if($('#size').length > 0) {
    $("#size").slider();
    $("#size").on("slide", function(slideEvt) {
        $("#sizeVal").text(slideEvt.value);
    });
}
if($('#teinte').length > 0) {
    $("#teinte").slider();
    $("#teinte").on("slide", function(slideEvt) {
        $("#teinteVal").text(slideEvt.value);
    });
}
if($('#saturation').length > 0) {
    $("#saturation").slider();
    $("#saturation").on("slide", function(slideEvt) {
        $("#saturationVal").text(slideEvt.value);
    });
}
if($('#lum').length > 0) {
    $("#lum").slider();
    $("#lum").on("slide", function(slideEvt) {
        $("#lumVal").text(slideEvt.value);
    });
}
if($('#control').length > 0) {
    $("#control").slider();
    $("#control").on("slide", function(slideEvt) {
        $("#controlval").text(slideEvt.value);
    });
}
if($('#invers').length > 0) {
    $("#invers").slider();
    $("#invers").on("slide", function(slideEvt) {
        $("#inversVal").text(slideEvt.value);
    });
}
if($('#sepia').length > 0) {
    $("#sepia").slider();
    $("#sepia").on("slide", function(slideEvt) {
        $("#sepiaVal").text(slideEvt.value);
    });
}
if($('#opacity').length > 0) {
    $("#opacity").slider();
    $("#opacity").on("slide", function(slideEvt) {
        $("#opacityVal").text(slideEvt.value);
    });
}
if($('#resum').length > 0) {
    $("#resum").slider();
    $("#resum").on("slide", function(slideEvt) {
        $("#resumVAl").text(slideEvt.value);
    });
}

$('.list-btn .nav-item a').click(function() {
    $('.pcr-app').removeClass('gradient');

    if($(this).attr('href') !="#palette-tab") {
        $('.pcr-app').removeClass('visible');
    } else {
        $('.pcr-app').addClass('visible');
    }
});
$('#badge-quiz').on("change", function(e) {
    var d= $("#badge-quiz").select2('val');
    console.log(d);
    if (d=="Certificats"){
        $('#quiz-dev').removeClass('hidden')
        $('#quiz-5').addClass('hidden');
    }else {
        $('#quiz-dev').addClass('hidden');
        $('#quiz-5').removeClass('hidden');
    }
    if(d=="Autre"){
        $('#devoir-otr').removeClass('hidden');
        $('#dev-5').addClass('hidden');
        $('#devoir-val').addClass('hidden');
        $('#dev-color').addClass('hidden');
    }
});
$(".open-button").on("click", function() {
    $(this).closest('.collapse-group').find('.collapse').collapse('show');
});
if($(".select-filter").length > 0) {
    $(".select-filter").select2({
        minimumResultsForSearch: -1
    });
}

if($('#form_snippet_image').length) {
    Dropzone.autoDiscover = false;
    //je récupère l'action où sera traité l'upload en PHP
    var _actionToDropZone = $("#form_snippet_image2").attr('action');

    //je définis ma zone de drop grâce à l'ID de ma div citée plus haut.
    var myDropzone = new Dropzone("#form_snippet_image", { url: _actionToDropZone, success:function(result, responseStatus){
            var id  = responseStatus.id;
            var ids = $('#affect-file-folder #media-ids').val();
            $('#affect-file-folder #media-ids').val(ids+','+id);
        } });
    myDropzone.on("addedfile", function(file) {
        //we can add modale to indicate theat we have uploded a new file
        //alert('nouveau fichier reçu');
        $('#affect-file-folder').modal();
    });
    myDropzone.on("complete", function() {
        $.ajax({
            type: "POST",
            url: '/admin/getMedia',
            success: function(data) {
                $('#file-view').html(data);
            }
        });
    });
}
$('#add-file').click(function(e) {
    e.preventDefault();
    if($('.cutsom-dropzone').hasClass('hidden')) {
        $('.cutsom-dropzone').removeClass('hidden');
    } else {
        $('.cutsom-dropzone').addClass('hidden');
    }
})

$('#affect-filetofolder').click(function(e) {
    e.preventDefault();
    var folders  = $('#dropdown-folder').attr('data-ids');
    var filesIds = $('#media-ids').val();
    if(filesIds && folders) {
        $.ajax({
            type: "POST",
            url: '/admin/affectFolder',
            data: { folders: folders, filesIds:filesIds },
            success: function(data) {
                if (data == "success") {
                    $('#affect-file-folder').modal('hide');
                }
            }
        });
    } else {
        $('#affect-file-folder').modal('hide');
    }
});
$('.forma-btn').click(function() {
    console.log('click')
    $('#categ').removeClass('show');
    $('#forma').addClass("show");
    $('#new-forma').removeClass("hidden");
    $('#new-catag').addClass("hidden");
})
$('.categ-btn').click(function() {
    $('#forma').removeClass('show');
    $('#categ').addClass("show");
    $('#new-forma').addClass("hidden");
    $('#new-catag').removeClass("hidden");
});
$('#tab-categ').click(function() {
    $('.tab-categ').removeClass('hidden');
    $('.tab-formations').addClass('hidden');
    var activeImage = $(this).find('img').attr('data-activeimage');
    var nonActive = $('#tab-formations img').attr('data-nonactiveimage');
    $(this).find('img').attr('src', nonActive);
    $('#tab-formations img').attr('src', activeImage);
});
$('#tab-formations').click(function() {
    $('.tab-formations').removeClass('hidden');
    $('.tab-categ').addClass('hidden');
    var activeImage = $(this).find('img').attr('data-activeimage');
    var nonActive = $('#tab-formations img').attr('data-nonactiveimage');
    $(this).find('img').attr('src', activeImage);
    $('#tab-categ img').attr('src', nonActive);
});
$('.del-bad').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('data-id');

    console.log(id);
    $.ajax({
        type: "POST",
        data: {id: id},
        url: '/formation/deleteBadge',
        success: function (data) {
            location.reload();
        }
    });
});
var img = $('.vich-image-badge a').attr('href');

if (img) {
    $('.img-badge').attr('src', img);
}
function filePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.vich-image-badge  img, .img-badge').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#badge_picture_file").change(function() {
    filePreview(this);
});
$('#typeFC').on("change", function(e) {
    var d= $("#typeFC").select2('val');
    if (d=="Ajouter à une/des classe(s)"){
        $('#classe').removeClass('hidden')
        $('#formationS').addClass('hidden');
    }else {
        $('#classe').addClass('hidden');
        $('#formationS').removeClass('hidden');
    }
});

$("#getvalforinput").bind("change ", function() {
    var s=$("#getvalforinput input[type='radio']:checked").val();
    $(".attributeval").val(s);
})
$("#getsensbase").bind("change ", function() {
    var s=$("#getsensbase input[type='radio']:checked").val();
    $(".savesensjs").val(s);
})

$("#choose-folder").bind("change ", function() {
    var getids=$("#choose-folder").attr("data-ids");
    $("#formationconfig_preqformation").val(getids);

})


function checkboxDropdowns(el='', text="Ajouter au(x) dossier(s)") {
    var $el = $(el)

    function updateStatus(label, result, folders) {
        if(!result.length) {
            //label.html(text);
        }
    };

    $el.each(function(i, element) {
        var $list = $(this).find('.dropdown-forum'),
            $label = $(this).find('.dropdown-labelforum '),
            $inputs = $(this).find('.check'),
            defaultChecked = $(this).find('input[type=checkbox]:checked'),
            result = [];
        folders = [];
        htmlResult = [];
        htmlResult2 = [];

        updateStatus($label, result);
        if(defaultChecked.length) {
            defaultChecked.each(function () {
                if($(this).parent().parent().hasClass('catalogue-list')) {
                    //htmlResult.push('<span>'+$(this).next().text()+'</span>');

                } else {
                    htmlResult2.push('<span>'+$(this).next().text()+'</span>');
                }
                result.push($(this).next().text());
                htmlResult.push('<span>'+$(this).next().text()+'</span>');


                if(el != ".dropdown-selectforum") {
                    $label.html(result.join(", "));
                }else {
                    if($(this).parent().parent().hasClass('catalogue-list')){
                        $('.catalogue .list-selection').html(htmlResult.join(" "));


                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));

                    }

                }

                folders.push($(this).next().attr('data-id'));
            });
        }

        $label.on('click', ()=> {
            $(this).toggleClass('open');
        });

        $inputs.on('change', function() {
            var checked = $(this).is(':checked');
            var checkedText = $(this).next().text();
            var checkedID= $(this).next().attr('data-id');
            if(checked) {
                result.push(checkedText);
                if($(this).parent().parent().hasClass('catalogue-list')) {
                    htmlResult.push('<span>'+checkedText+'</span>');
                }else {
                    htmlResult2.push('<span>'+checkedText+'</span>');
                }


                if(el != ".dropdown-selectforum") {
                    $label.html(result.join(", "));
                }else {
                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        $('.catalogue .list-selection').html(htmlResult.join(" "));
                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));
                    }
                }

                folders.push(checkedID);
            }else{
                let index = result.indexOf(checkedText);
                if (index >= 0) {
                    result.splice(index, 1);
                    folders.splice(index, 1);

                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        htmlResult.splice(index, 1);
                    }else {
                        htmlResult2.splice(index, 1);
                    }
                }
                if(el != ".dropdown-selectforum") {
                    $label.html(result.join(", "));
                } else {
                    if($(this).parent().parent().hasClass('catalogue-list')) {
                        $('.catalogue .list-selection').html(htmlResult.join(" "));
                    } else {
                        $('.category .list-selection').html(htmlResult2.join(" "));
                    }

                }
            }
            updateStatus($label, result, folders);
            $('#formdrown').attr('data-ids', folders);
        });
    });
};


$("#formdrown").bind("change ", function() {

    var getidsnew=$("#formdrown").attr("data-ids");

    $("#formationconfig_insertjoinform").val(getidsnew);

})

$('#checkAllf').change(function() {
    // this will contain a reference to the checkbox
    if (this.checked) {
        // the checkbox is now checked
        $('.check-element').prop("checked", true);
    } else {
        // the checkbox is now no longer checked
        $('.check-element').prop("checked", false);
    }
});
$('#delete-categories').click(function () {
    //get all selected categories
    var categories = [];
    $.each($("input[name='categorie-check']:checked"), function () {
        categories.push($(this).attr('data-id'));
    });
    console.log(categories);
    var path = $(this).attr("data-path");
    console.log(path);
    $.ajax({
        type: "POST",
        url: '/formation/deleteCategories',
        data: {categories: categories},
        success: function (data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
});
$('#tab-categories').click(function() {
    $('.tab-categories').removeClass('hidden');
    $('.tab-categ').addClass('hidden');
    $('.list-categories').addClass('hidden');
    var activeImage = $(this).find('img').attr('data-activeimage');
    var nonActive = $('#list-categories img').attr('data-nonactiveimage');
    $(this).find('img').attr('src', activeImage);
    $('#list-categories img').attr('src', nonActive);

});

$('#list-categories').click(function() {
    $('.tab-categ').removeClass('hidden');
    $('.tab-categories').addClass('hidden');
    var activeImage = $(this).find('img').attr('data-activeimage');
    var nonActive = $('#tab-categories img').attr('data-nonactiveimage');
    $(this).find('img').attr('src', activeImage);
    $('#tab-categories img').attr('src', nonActive);
});
$('.delete-cat').click(function (e) {
    $('.delete-cat').removeClass('active');
    $(this).addClass('active');
})
$('.del-categ').click(function (e) {
    e.preventDefault();
    var id = $('.delete-cat.active').attr('data-id');
    console.log(id);
    $.ajax({
        type: "POST",
        data: {id: id},
        url: '/formation/deleteCategory',
        success: function (data) {
            if(data == 'success'){
                location.reload();
            }
        }
    });
});
$('.delete-forma').click(function (e) {
    $('.delete-forma').removeClass('active');
    $(this).addClass('active');
})
$('.del-forma').click(function (e) {
    e.preventDefault();
    var id = $('.delete-forma.active').attr('data-id');
    console.log(id);
    $.ajax({
        type: "POST",
        data: {id: id},
        url: '/formation/deleteFormation',
        success: function (data) {
            if(data == 'success'){
                location.reload();
            }
        }
    });
});
var formations = [];
$('#delete-formas').click(function () {
    //get all selected formations

    $.each($("input[name='formation-check']:checked"), function () {
        formations.push($(this).attr('data-id'));
    });
    console.log(formations);
    var path = $(this).attr("data-path");
    $.ajax({
        type: "POST",
        url: '/formation/deleteFormations',
        data: {formations: formations},
        success: function (data) {
            if (data == "success") {
                location.reload();
            }
        }
    });
});
/*$('#categorys').on("change", function(e) {
    if ($("#categorys").length > 0) {
        var s = $("#categorys").val();
        console.log(s);
        if (s == 'INFORMATIQUE') {
        }
    }
});*/

$("#choose-folder").bind("change ", function() {

    var getidsnew=$("#choose-folder").attr("data-ids");
    var editcoursnew=$("#choose-folder").attr("data-ids");
    $("#choice_preq").val(getidsnew);
$("#select_choiceprerequis").val(editcoursnew);
})
